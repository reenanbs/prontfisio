angular.module("prontFisio").controller("indexAdminCtrl",function($scope, $http){
	$scope.newRecordPhysiotherapist = false;
	$scope.listPhysiotherapist = false;
	$scope.newPhysiotherapist = function(){
		$scope.physiotherapist = {};
		$scope.newRecordPhysiotherapist = !$scope.newRecordPhysiotherapist;
		$scope.view = false;
	}
	
	$scope.listPhysio = function(){
		findAllPhysiotherapists();
		$scope.listPhysiotherapist = !$scope.listPhysiotherapist;
		$scope.view = false;
	}
	
	$scope.dashboard = function(){
		$scope.newRecordPhysiotherapist = false;
		$scope.listPhysiotherapist = false;
		$scope.view = false;
	}
	
	
	init();
	$scope.edit =false;

	function init(){
		findAllPhysiotherapists();
		findAllClinicas();
	};
	
	$scope.insert = function(physiotherapist){
		$http.post('../rest/physiotherapist/insertPhysiotherapist', physiotherapist)
		.then(function(response) {
			$scope.message = "Fisioterapeuta Inserido com sucesso!"; 
			$scope.physiotherapist = {};
			$scope.dashboard();
		}
		,function(error){
			
		})
	};
	
	function findAllPhysiotherapists(){
		$http.get('../rest/physiotherapist/findAllPhysiotherapist')
		.then(function(response){
			$scope.physiotherapists = response.data;
		}
		,function(error){
			
		})
	};
	
	function findAllClinicas(){
		$http.get('../rest/physiotherapist/findAllClinicas')
		.then(function(response){
			$scope.clinicas = response.data;
		}
		,function(error){
			
		})
	};
	
	$scope.cancel = function(){
		$scope.edit = false;
	};
	
	$scope.editPhysiotherapist = function(physiotherapist){
		$scope.physiotherapist = physiotherapist;
		$scope.newRecordPhysiotherapist = true;
		$scope.edit = true;
		$scope.view = false;
	};
	
	$scope.viewPhysiotherapist = function(physiotherapist){
		$scope.physiotherapist = physiotherapist;
		$scope.listPhysiotherapist = false;
		$scope.view = true;
	};
	
	$scope.update = function(physiotherapist){
		$http.put('../rest/physiotherapist/updatePhysiotherapist',physiotherapist)
		.then(function(response){
			$scope.edit = false;
			$scope.physiotherapists = response.data.physiotherapists;
			$scope.dashboard();
		},function(error){
			
		})
	};
	
	$scope.remove = function(physiotherapist){
		$http.delete('../rest/physiotherapist/deletePhysiotherapist/'+physiotherapist.id)
		.then(function(response){
			$scope.physiotherapists = response.data.physiotherapists;
		},function(error){
			
		})
	};

});