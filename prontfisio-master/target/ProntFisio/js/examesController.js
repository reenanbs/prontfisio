	
angular.module("prontFisio")
	.controller("examesCtrl",function($scope, $http){
	init();

	function init(){
		findAllPatients();
	};
	
	function findAllPatients(){
		$http.get('rest/patient/findAllPatient')
		.then(function(response){
			$scope.patients = response.data;
		}
		,function(error){
			
		})
	};

	$scope.changePatient = function(){
		initVariables();
	}
	

	function initVariables(){
		$scope.patientSelected.examesFisicos = $scope.patientSelected.examesFisicos ? $scope.patientSelected.examesFisicos : {};
		$scope.patientSelected.examesFisicos.avaliacaoFisica = $scope.patientSelected.examesFisicos.avaliacaoFisica ? $scope.patientSelected.examesFisicos.avaliacaoFisica : {};
		$scope.patientSelected.examesFisicos.goniometria = $scope.patientSelected.examesFisicos.goniometria ? $scope.patientSelected.examesFisicos.goniometria : {};
		$scope.patientSelected.examesFisicos.goniometria.pescoco = $scope.patientSelected.examesFisicos.goniometria.pescoco ? $scope.patientSelected.examesFisicos.goniometria.pescoco : {};
		$scope.patientSelected.examesFisicos.goniometria.coluna = $scope.patientSelected.examesFisicos.goniometria.coluna ? $scope.patientSelected.examesFisicos.goniometria.coluna : {};
		$scope.patientSelected.examesFisicos.goniometria.ombro = $scope.patientSelected.examesFisicos.goniometria.ombro ? $scope.patientSelected.examesFisicos.goniometria.ombro : {};
		$scope.patientSelected.examesFisicos.goniometria.cotovelo = $scope.patientSelected.examesFisicos.goniometria.cotovelo ? $scope.patientSelected.examesFisicos.goniometria.cotovelo : {};
		$scope.patientSelected.examesFisicos.goniometria.punho = $scope.patientSelected.examesFisicos.goniometria.punho ? $scope.patientSelected.examesFisicos.goniometria.punho : {};
		$scope.patientSelected.examesFisicos.goniometria.quadril = $scope.patientSelected.examesFisicos.goniometria.quadril ? $scope.patientSelected.examesFisicos.goniometria.quadril : {};
		$scope.patientSelected.examesFisicos.goniometria.joelho = $scope.patientSelected.examesFisicos.goniometria.joelho ? $scope.patientSelected.examesFisicos.goniometria.joelho : {};
		$scope.patientSelected.examesFisicos.goniometria.tornozelo = $scope.patientSelected.examesFisicos.goniometria.tornozelo ? $scope.patientSelected.examesFisicos.goniometria.tornozelo : {};
		$scope.patientSelected.examesFisicos.forcaMuscular = $scope.patientSelected.examesFisicos.forcaMuscular ? $scope.patientSelected.examesFisicos.forcaMuscular : {};
		$scope.patientSelected.examesFisicos.avaliacaoPostural = $scope.patientSelected.examesFisicos.avaliacaoPostural ? $scope.patientSelected.examesFisicos.avaliacaoPostural : {};
		$scope.patientSelected.examesFisicos.avaliacaoEquilibrio = $scope.patientSelected.examesFisicos.avaliacaoEquilibrio ? $scope.patientSelected.examesFisicos.avaliacaoEquilibrio : {};
		$scope.patientSelected.examesFisicos.avaliacaoMarcha = $scope.patientSelected.examesFisicos.avaliacaoMarcha ? $scope.patientSelected.examesFisicos.avaliacaoMarcha : {};
		$scope.patientSelected.examesFisicos.escalaVisualDeDor = $scope.patientSelected.examesFisicos.escalaVisualDeDor ? $scope.patientSelected.examesFisicos.escalaVisualDeDor : {};
		$scope.patientSelected.examesFisicos.questionarioBasal = $scope.patientSelected.examesFisicos.questionarioBasal ? $scope.patientSelected.examesFisicos.questionarioBasal : {};
		$scope.patientSelected.examesFisicos.questionarioBasal.ocupacao = $scope.patientSelected.examesFisicos.questionarioBasal.ocupacao ? $scope.patientSelected.examesFisicos.questionarioBasal.ocupacao : {};
		$scope.patientSelected.examesFisicos.questionarioBasal.esporte =  $scope.patientSelected.examesFisicos.questionarioBasal.esporte ? $scope.patientSelected.examesFisicos.questionarioBasal.esporte :{};
		$scope.patientSelected.examesFisicos.questionarioBasal.lazer = $scope.patientSelected.examesFisicos.questionarioBasal.lazer ? $scope.patientSelected.examesFisicos.questionarioBasal.lazer : {};
		$scope.patientSelected.examesFisicos.questionarioBasal.sumario = $scope.patientSelected.examesFisicos.questionarioBasal.sumario ? $scope.patientSelected.examesFisicos.questionarioBasal.sumario :{};
		parseDate();
	}
	
	function parseDate(){
		if($scope.patientSelected.examesFisicos.avaliacaoFisica.dataAvaliacao != null){
			$scope.patientSelected.examesFisicos.avaliacaoFisica.dataAvaliacao = new Date($scope.patientSelected.examesFisicos.avaliacaoFisica.dataAvaliacao);
			$scope.patientSelected.examesFisicos.avaliacaoFisica.dataAvaliacao.setHours($scope.patientSelected.examesFisicos.avaliacaoFisica.dataAvaliacao.getHours() + 3);
		}
		
	}
	
	$scope.update = function(patient){
		$http.put('rest/patient/updatePatient',patient)
		.then(function(response){
			$scope.patients = response.data.patients;
			$scope.patientSelected = null;
		},function(error){
			
		})
	};
	
});


