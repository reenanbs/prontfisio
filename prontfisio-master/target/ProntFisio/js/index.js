angular.module("prontFisio").controller("appointmentBookCtrl",function($scope, $http){
	init();
	
	function init(){
		/*findAllPatients();*/
		search();
	};
	
	function findAllPatients(){
		$http.get('rest/patient/findAllPatient')
		.then(function(response){
			$scope.patients = response.data;
		}
		,function(error){
			
		})
	};
	
	function search(){
		$http.get('rest/patient/search')
		.then(function(response){
			$scope.patients = response.data.patients;
			addEventos();
		}
		,function(error){
			
		})
	};
	
	
	function addEventos(){
		$scope.events =[];
		var hour =0;
		angular.forEach($scope.patients,function(patient){
			var evento = {};
			evento.id = patient.id;
			evento.text = patient.name;
			evento.start = new Date(patient.evaluationDate);
			evento.start.setHours(evento.start.getHours() + 9 + hour);
			evento.end = new Date(patient.evaluationDate);
			evento.end.setHours(evento.end.getHours() + 10 + hour);
			$scope.events.push(evento);
			hour +=1;
		});
		
	}

	/*new DayPilot.Date("2018-04-27T13:30:00").toString("dd/MM/yyyy");*/
	
	/*$scope.events = [
		  {
			    "id":"12",
			    "text":"Atendimento 1",
			    "start":"2018-04-25T12:00:00",
			    "end":"2018-04-25T16:30:00"
			  },
			  {
				    "id":"15",
				    "text":"Atendimento 2",
				    "start":"2018-04-26T09:00:00",
				    "end":"2018-04-26T11:30:00"
				  }
			];*/

    $scope.weekConfig = {
        viewType: "Week",
        theme:"calendar_green",
        headerDateFormat:"dd/MM/yyyy"
    };
	
	
	

});