	
angular.module("prontFisio")
	.controller("patientCtrl",function($scope, $http){
	init();
	$scope.edit =false;
	$scope.view = false;

	function init(){
		findAllPatients();
	};
	
	$scope.insert = function(patient){
		$http.post('rest/patient/insertPatient', patient)
		.then(function(response) {
			$scope.message = "Paciente Inserido com sucesso!"; 
			$scope.patient = {};
		}
		,function(error){
			
		})
	};
	
	
	$scope.calcularIMC = function(peso, altura){
		$scope.patient.antropometria.imc = Math.round(peso /(altura * altura ));
	}
	
	function findAllPatients(){
		$http.get('rest/patient/findAllPatient')
		.then(function(response){
			$scope.patients = response.data;
			parseDate();
		}
		,function(error){
			
		})
	};
	
	function parseDate(){
		angular.forEach($scope.patients,function(patient){
			patient.dateOfBirth = new Date(patient.dateOfBirth);
			patient.dateOfBirth.setHours(patient.dateOfBirth.getHours() + 3);
			patient.evaluationDate = new Date(patient.evaluationDate);
			patient.evaluationDate.setHours(patient.evaluationDate.getHours() + 3);
		});
	}
	
	
	$scope.cancel = function(){
		$scope.edit = false;
	};
	
	$scope.editPatient = function(patient){
		$scope.patient = patient;
		$scope.edit = true;
	};
	
	$scope.viewPatient = function(patient){
		$scope.patient = patient;
		$scope.edit = false;
		$scope.view = true;
	};
	
	$scope.update = function(patient){
		$http.put('rest/patient/updatePatient',patient)
		.then(function(response){
			$scope.edit = false;
			$scope.patients = response.data.patients;
		},function(error){
			
		})
	};
	
	$scope.remove = function(patient){
		$http.delete('rest/patient/deletePatient/'+patient.id)
		.then(function(response){
			$scope.patients = response.data.patients;
		},function(error){
			
		})
	};
});


