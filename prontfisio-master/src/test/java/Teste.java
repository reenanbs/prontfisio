

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Teste {
	
	public static void main(String[] args) {
		EntityManagerFactory fac = Persistence.createEntityManagerFactory("prontFisio");
		EntityManager manager = fac.createEntityManager();
		
		
		manager.getTransaction().begin();
		manager.getTransaction().commit();
		manager.close();
		
	}
}
