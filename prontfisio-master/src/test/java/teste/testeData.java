package teste;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import com.prontfisio.helper.DateHelper;

public class testeData {
	
	@Test
	public void testeSemana() {
		DateFormat df = new SimpleDateFormat ("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek (Calendar.SUNDAY);
		// Para pegar o primeiro dia desta semana, vamos ver que dia da semana é hoje, e subtrair
		// o número de dias a partir de domingo.
		// Note que a semana pode começar no mês passado.
		int diaSemana = cal.get(Calendar.DAY_OF_WEEK);
		cal.add (Calendar.DAY_OF_MONTH, Calendar.SUNDAY - diaSemana);
		System.out.println (df.format (cal.getTime()));
		// Para pegar o último dia deste mês:
		cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, diaSemana - Calendar.SATURDAY);
		System.out.println (df.format (cal.getTime()));
		
	}
	
	@Test
	public void primeiroDia() {
		DateFormat df = new SimpleDateFormat ("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek (Calendar.SUNDAY);
		int diaSemana = cal.get(Calendar.DAY_OF_WEEK);
		cal.add (Calendar.DAY_OF_MONTH, Calendar.SUNDAY - diaSemana);
		System.out.println (df.format (cal.getTime()));
	}
	
	@Test
	public void ultimoDia() {
		DateFormat df = new SimpleDateFormat ("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek (Calendar.SUNDAY);
		int diaSemana = cal.get(Calendar.DAY_OF_WEEK);
		cal.add(Calendar.DAY_OF_MONTH, Calendar.SUNDAY + diaSemana);
		System.out.println (df.format (cal.getTime()));
	}
	
	@Test
	public void testMethod() {
		DateHelper dateHelper = new DateHelper();
		System.out.println(dateHelper.getFirstOrLastDayOfWeekByDate(new Date(), true));
		System.out.println(dateHelper.getFirstOrLastDayOfWeekByDate(new Date(), false));
	}
	
	@Test
	public void testMethod2() {
		DateHelper dateHelper = new DateHelper();
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2018);
		calendar.set(Calendar.MONTH, Calendar.MAY);
		calendar.set(Calendar.DAY_OF_MONTH, 5);
		System.out.println(dateHelper.getFirstOrLastDayOfWeekByDate(calendar.getTime(), true));
		System.out.println(dateHelper.getFirstOrLastDayOfWeekByDate(calendar.getTime(), false));
	}
	
	@Test
	public void calendarTest() {
		DateFormat df = new SimpleDateFormat ("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.SUNDAY);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		System.out.println (df.format (cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		System.out.println (df.format (cal.getTime()));
	}
	
}
