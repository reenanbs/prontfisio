	
angular.module("prontFisio")
	.controller("patientCtrl",function($scope, $http){
	init();
	$scope.edit =false;
	$scope.view = false;

	function init(){
		findAllPatients();
	};
	
	$scope.insert = function(patient){
		$http.post('rest/patient/insertPatient', patient)
		.then(function(response) {
			$scope.message = "Paciente Inserido com sucesso!"; 
			$scope.patient = {};
		}
		,function(error){
			$scope.message = "Operação falhou";
		})
	};
	
	
	$scope.calcularIMC = function(peso, altura){
		var result = "";
		var risco = "";
		$scope.patient.antropometria.imc = Math.round(peso /(altura * altura ));
		if($scope.patient.antropometria.imc < 18.5){
			result = "Baixo Peso";
			risco = "Baixo";
		} else if($scope.patient.antropometria.imc < 25){
			result = "Peso normal";
			risco = "Médio";
		} else if($scope.patient.antropometria.imc < 30){
			result = "Paciente com sobrepeso - Pré-obeso";
			risco = "Aumentado";
		} else if($scope.patient.antropometria.imc < 35){
			result = "Paciente com sobrepeso - Obeso I";
			risco = "Moderado";
		} else if($scope.patient.antropometria.imc < 40){
			result = "Paciente com sobrepeso - Obeso II";
			risco = "Grave";
		} else {
			result = "Paciente com sobrepeso - Obeso III";
			risco = "Muito Grave";
		}
		$scope.messageImc="Classificação: "+ result + " - Risco de Comorbidades: "+ risco;
	}
	
	function findAllPatients(){
		$http.get('rest/patient/findAllPatient')
		.then(function(response){
			$scope.patients = response.data;
			parseDate();
			$scope.message = null;
		}
		,function(error){
			$scope.message = "Operação falhou";
		})
	};
	
	function parseDate(){
		angular.forEach($scope.patients,function(patient){
			patient.dateOfBirth = new Date(patient.dateOfBirth);
			patient.dateOfBirth.setHours(patient.dateOfBirth.getHours() + 3);
			patient.evaluationDate = new Date(patient.evaluationDate);
			patient.evaluationDate.setHours(patient.evaluationDate.getHours() + 3);
		});
	}
	
	
	$scope.cancel = function(){
		$scope.edit = false;
	};
	
	$scope.editPatient = function(patient){
		$scope.patient = patient;
		$scope.edit = true;
	};
	
	$scope.viewPatient = function(patient){
		$scope.patient = patient;
		$scope.edit = false;
		$scope.view = true;
	};
	
	$scope.update = function(patient){
		$http.put('rest/patient/updatePatient',patient)
		.then(function(response){
			$scope.edit = false;
			$scope.patients = response.data.patients;
			$scope.message="Paciente Atualizado com sucesso";
		},function(error){
			$scope.message = "Operação falhou";
		})
	};
	
	$scope.remove = function(patient){
		$http.delete('rest/patient/deletePatient/'+patient.id)
		.then(function(response){
			$scope.patients = response.data.patients;
			$scope.message = "Paciente removido com sucesso";
		},function(error){
			$scope.message = "Operação falhou";
		})
	};
});


