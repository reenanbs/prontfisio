angular.module("prontFisio").controller("appointmentBookCtrl",function($scope, $http){
	init();
	
	function init(){
		search();
	};
	
	$scope.update = function(user){
		if(passwordsAreEquals(user)){
			delete user.password;
			$http.put('rest/user/update',user)
			.then(function(response){
				
			}
			,function(error){
				
			})
		} else{
			
		}
		
	};
	
	function passwordsAreEquals(user){
		if(user.senha === user.password){
			return true;
		}
		return false;
	}
	
	function search(){
		$http.get('rest/patient/search')
		.then(function(response){
			$scope.patients = response.data.patients;
			addEventos();
		}
		,function(error){
			
		})
	};
	
	function searchByDate(data){
		$http.post('rest/patient/searchByDate',{date : data})
		.then(function(response){
			$scope.patients = response.data.patients;
			addEventos();
		}
		,function(error){
			
		})
	};
	
	function addEventos(){
		$scope.events =[];
		var hour =0;
		angular.forEach($scope.patients,function(patient){
			var evento = {};
			evento.id = patient.id;
			evento.text = patient.name;
			evento.start = new Date(patient.evaluationDate);
			evento.start.setHours(evento.start.getHours() + 9 + hour);
			evento.end = new Date(patient.evaluationDate);
			evento.end.setHours(evento.end.getHours() + 10 + hour);
			$scope.events.push(evento);
			hour +=1;
		});
		
	}

	/*new DayPilot.Date("2018-04-27T13:30:00").toString("dd/MM/yyyy");*/
	
	/*$scope.events = [
		  {
			    "id":"12",
			    "text":"Atendimento 1",
			    "start":"2018-04-25T12:00:00",
			    "end":"2018-04-25T16:30:00"
			  },
			  {
				    "id":"15",
				    "text":"Atendimento 2",
				    "start":"2018-04-26T09:00:00",
				    "end":"2018-04-26T11:30:00"
				  }
			];*/

    $scope.weekConfig = {
        viewType: "Week",
        theme:"calendar_green",
        headerDateFormat:"dd/MM/yyyy",
        visible:true,
        businessBeginsHour: 8,
        businessEndsHour: 18,
        timeFormat : "Auto",
        locale:"pt-br"
    };
	
    $scope.dayConfig = {
    	viewType: "Day",
        theme:"calendar_green",
        headerDateFormat:"dd/MM/yyyy",
        visible:false,
        businessBeginsHour: 8,
        businessEndsHour: 18,
        timeFormat : "Auto",
        locale:"pt-br"
    };
    
    $scope.showDay = function() {
    	$scope.dayConfig.visible = true;
    	$scope.weekConfig.visible = false;  
    };

    $scope.showWeek = function() {
    	$scope.dayConfig.visible = false;
    	$scope.weekConfig.visible = true;                    
    };
	
    $scope.navigatorConfig = {
    	locale:"pt-br",
        selectMode: "day",
        showMonths: 2,
        skipMonths: 2,
        onTimeRangeSelected: function(args) {
        	var date = new Date();
            $scope.weekConfig.startDate = args.day;
            $scope.dayConfig.startDate = args.day;                            
            if($scope.dayConfig.visible){
            	date = new Date($scope.dayConfig.startDate); 
            } else {
            	date = new Date($scope.weekConfig.startDate);
            }
            searchByDate(date);
         }
    };
});