	
angular.module("prontFisio")
	.controller("examesCtrl",function($scope, $http){
	init();

	function init(){
		findAllPatients();
	};
	
	function findAllPatients(){
		$http.get('rest/patient/findAllPatient')
		.then(function(response){
			$scope.patients = response.data;
		}
		,function(error){
			
		})
	};
	
	$scope.listenerLazer = function(){
		if($scope.patientSelected.examesFisicos.questionarioBasal.lazer.tv != null &&
				$scope.patientSelected.examesFisicos.questionarioBasal.lazer.andaPe != null &&
				$scope.patientSelected.examesFisicos.questionarioBasal.lazer.andaBicicleta != null &&
				$scope.patientSelected.examesFisicos.questionarioBasal.lazer.minutosAndando){
			var value = ((6 - parseInt($scope.patientSelected.examesFisicos.questionarioBasal.lazer.tv))+
					parseInt($scope.patientSelected.examesFisicos.questionarioBasal.lazer.andaPe)+
					parseInt($scope.patientSelected.examesFisicos.questionarioBasal.lazer.andaBicicleta)+
					parseInt($scope.patientSelected.examesFisicos.questionarioBasal.lazer.minutosAndando)) / 4;
			$scope.patientSelected.examesFisicos.questionarioBasal.lazer.calculoIndiceAtividade = value.toString();
			$scope.patientSelected.examesFisicos.questionarioBasal.sumario.atividadeLazer = $scope.patientSelected.examesFisicos.questionarioBasal.lazer.calculoIndiceAtividade; 
		}
		
	}
	
	
	$scope.listenerOcupacao = function(){
		if($scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.principalOcupacao != null &&
		$scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.sentaTrabalho != null &&
		$scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.peTrabalho != null &&
		$scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.andaTrabalho != null &&
		$scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.carregaObjetosPesadosTrabalho != null &&
		$scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.cansado != null &&
		$scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.suor != null &&
		$scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.trabalhoFisicamente){
			var value = (parseInt($scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.principalOcupacao)+
			(6 - parseInt($scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.sentaTrabalho))+
			parseInt($scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.peTrabalho)+
			parseInt($scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.andaTrabalho)+
			parseInt($scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.carregaObjetosPesadosTrabalho)+
			parseInt($scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.cansado)+
			parseInt($scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.suor)+
			parseInt($scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.trabalhoFisicamente))/8
			
			$scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.totalIndiceOcupacional = value.toString();
			
			$scope.patientSelected.examesFisicos.questionarioBasal.sumario.ocupacional = $scope.patientSelected.examesFisicos.questionarioBasal.ocupacao.totalIndiceOcupacional; 
		}
	}
	
	$scope.listenerAvaliacaoMarcha = function(){
	
		if($scope.patientSelected.examesFisicos.avaliacaoMarcha.inicioMarcha != null &&
		$scope.patientSelected.examesFisicos.avaliacaoMarcha.balanceioPernaDireitaPassa != null &&
		$scope.patientSelected.examesFisicos.avaliacaoMarcha.balanceioPernaDireitaAfasta != null &&
		$scope.patientSelected.examesFisicos.avaliacaoMarcha.balanceioPernaEsquerdaPassa != null &&
		$scope.patientSelected.examesFisicos.avaliacaoMarcha.balanceioPernaEsquerdaAfasta != null &&
		$scope.patientSelected.examesFisicos.avaliacaoMarcha.simetriaPassos != null &&
		$scope.patientSelected.examesFisicos.avaliacaoMarcha.continuidadePassos != null &&
		$scope.patientSelected.examesFisicos.avaliacaoMarcha.desvio != null &&
		$scope.patientSelected.examesFisicos.avaliacaoMarcha.tronco != null &&
		$scope.patientSelected.examesFisicos.avaliacaoMarcha.baseApoio){
			var value = parseInt($scope.patientSelected.examesFisicos.avaliacaoMarcha.inicioMarcha)+
			parseInt($scope.patientSelected.examesFisicos.avaliacaoMarcha.balanceioPernaDireitaPassa)+
			parseInt($scope.patientSelected.examesFisicos.avaliacaoMarcha.balanceioPernaDireitaAfasta)+
			parseInt($scope.patientSelected.examesFisicos.avaliacaoMarcha.balanceioPernaEsquerdaPassa)+
			parseInt($scope.patientSelected.examesFisicos.avaliacaoMarcha.balanceioPernaEsquerdaAfasta)+
			parseInt($scope.patientSelected.examesFisicos.avaliacaoMarcha.simetriaPassos)+
			parseInt($scope.patientSelected.examesFisicos.avaliacaoMarcha.continuidadePassos)+
			parseInt($scope.patientSelected.examesFisicos.avaliacaoMarcha.desvio)+
			parseInt($scope.patientSelected.examesFisicos.avaliacaoMarcha.tronco)+
			parseInt($scope.patientSelected.examesFisicos.avaliacaoMarcha.baseApoio)
			
			
			$scope.patientSelected.examesFisicos.avaliacaoMarcha.escoreMarcha = value.toString();
			
			somaDasAvaliacoes();
		}
		
	}
	
	function somaDasAvaliacoes(){
		if($scope.patientSelected.examesFisicos.avaliacaoEquilibrio.total != null && $scope.patientSelected.examesFisicos.avaliacaoMarcha.escoreMarcha != null){
			var value= parseInt($scope.patientSelected.examesFisicos.avaliacaoEquilibrio.total) + parseInt($scope.patientSelected.examesFisicos.avaliacaoMarcha.escoreMarcha);
			$scope.patientSelected.examesFisicos.avaliacaoMarcha.escoreTotal = value.toString();
		}
	}
	
	
	$scope.listenerAvaliacaoEquilibrio = function(){
		if($scope.patientSelected.examesFisicos.avaliacaoEquilibrio.equilibrioSentado != null && $scope.patientSelected.examesFisicos.avaliacaoEquilibrio.levantaDaCadeira != null &&
			$scope.patientSelected.examesFisicos.avaliacaoEquilibrio.tentativasLevantar != null && $scope.patientSelected.examesFisicos.avaliacaoEquilibrio.equilibrioImediato != null &&
			$scope.patientSelected.examesFisicos.avaliacaoEquilibrio.equilibrioPe != null && $scope.patientSelected.examesFisicos.avaliacaoEquilibrio.desequilibrio != null &&
			$scope.patientSelected.examesFisicos.avaliacaoEquilibrio.olhosFechados != null && $scope.patientSelected.examesFisicos.avaliacaoEquilibrio.girando != null &&
			$scope.patientSelected.examesFisicos.avaliacaoEquilibrio.sentar!= null){
			
			var value = parseInt($scope.patientSelected.examesFisicos.avaliacaoEquilibrio.equilibrioSentado) + parseInt($scope.patientSelected.examesFisicos.avaliacaoEquilibrio.levantaDaCadeira) +
			parseInt($scope.patientSelected.examesFisicos.avaliacaoEquilibrio.tentativasLevantar) + parseInt($scope.patientSelected.examesFisicos.avaliacaoEquilibrio.equilibrioImediato) +
			parseInt($scope.patientSelected.examesFisicos.avaliacaoEquilibrio.equilibrioPe) + parseInt($scope.patientSelected.examesFisicos.avaliacaoEquilibrio.desequilibrio) +
			parseInt($scope.patientSelected.examesFisicos.avaliacaoEquilibrio.olhosFechados) + parseInt($scope.patientSelected.examesFisicos.avaliacaoEquilibrio.girando) +
			parseInt($scope.patientSelected.examesFisicos.avaliacaoEquilibrio.sentar);
			$scope.patientSelected.examesFisicos.avaliacaoEquilibrio.total = value.toString();
			
			somaDasAvaliacoes();
		}
		
	}

	$scope.changePatient = function(patientSelected){
		$scope.patientSelected = patientSelected;
		$scope.message = null;
		initVariables();
	}
	

	function initVariables(){
		$scope.patientSelected.examesFisicos = $scope.patientSelected.examesFisicos ? $scope.patientSelected.examesFisicos : {};
		$scope.patientSelected.examesFisicos.avaliacaoFisica = $scope.patientSelected.examesFisicos.avaliacaoFisica ? $scope.patientSelected.examesFisicos.avaliacaoFisica : {};
		$scope.patientSelected.examesFisicos.goniometria = $scope.patientSelected.examesFisicos.goniometria ? $scope.patientSelected.examesFisicos.goniometria : {};
		$scope.patientSelected.examesFisicos.goniometria.pescoco = $scope.patientSelected.examesFisicos.goniometria.pescoco ? $scope.patientSelected.examesFisicos.goniometria.pescoco : {};
		$scope.patientSelected.examesFisicos.goniometria.coluna = $scope.patientSelected.examesFisicos.goniometria.coluna ? $scope.patientSelected.examesFisicos.goniometria.coluna : {};
		$scope.patientSelected.examesFisicos.goniometria.ombro = $scope.patientSelected.examesFisicos.goniometria.ombro ? $scope.patientSelected.examesFisicos.goniometria.ombro : {};
		$scope.patientSelected.examesFisicos.goniometria.cotovelo = $scope.patientSelected.examesFisicos.goniometria.cotovelo ? $scope.patientSelected.examesFisicos.goniometria.cotovelo : {};
		$scope.patientSelected.examesFisicos.goniometria.punho = $scope.patientSelected.examesFisicos.goniometria.punho ? $scope.patientSelected.examesFisicos.goniometria.punho : {};
		$scope.patientSelected.examesFisicos.goniometria.quadril = $scope.patientSelected.examesFisicos.goniometria.quadril ? $scope.patientSelected.examesFisicos.goniometria.quadril : {};
		$scope.patientSelected.examesFisicos.goniometria.joelho = $scope.patientSelected.examesFisicos.goniometria.joelho ? $scope.patientSelected.examesFisicos.goniometria.joelho : {};
		$scope.patientSelected.examesFisicos.goniometria.tornozelo = $scope.patientSelected.examesFisicos.goniometria.tornozelo ? $scope.patientSelected.examesFisicos.goniometria.tornozelo : {};
		$scope.patientSelected.examesFisicos.forcaMuscular = $scope.patientSelected.examesFisicos.forcaMuscular ? $scope.patientSelected.examesFisicos.forcaMuscular : {};
		$scope.patientSelected.examesFisicos.avaliacaoPostural = $scope.patientSelected.examesFisicos.avaliacaoPostural ? $scope.patientSelected.examesFisicos.avaliacaoPostural : {};
		$scope.patientSelected.examesFisicos.avaliacaoEquilibrio = $scope.patientSelected.examesFisicos.avaliacaoEquilibrio ? $scope.patientSelected.examesFisicos.avaliacaoEquilibrio : {};
		$scope.patientSelected.examesFisicos.avaliacaoMarcha = $scope.patientSelected.examesFisicos.avaliacaoMarcha ? $scope.patientSelected.examesFisicos.avaliacaoMarcha : {};
		$scope.patientSelected.examesFisicos.escalaVisualDeDor = $scope.patientSelected.examesFisicos.escalaVisualDeDor ? $scope.patientSelected.examesFisicos.escalaVisualDeDor : {};
		$scope.patientSelected.examesFisicos.questionarioBasal = $scope.patientSelected.examesFisicos.questionarioBasal ? $scope.patientSelected.examesFisicos.questionarioBasal : {};
		$scope.patientSelected.examesFisicos.questionarioBasal.ocupacao = $scope.patientSelected.examesFisicos.questionarioBasal.ocupacao ? $scope.patientSelected.examesFisicos.questionarioBasal.ocupacao : {};
		$scope.patientSelected.examesFisicos.questionarioBasal.esporte =  $scope.patientSelected.examesFisicos.questionarioBasal.esporte ? $scope.patientSelected.examesFisicos.questionarioBasal.esporte :{};
		$scope.patientSelected.examesFisicos.questionarioBasal.lazer = $scope.patientSelected.examesFisicos.questionarioBasal.lazer ? $scope.patientSelected.examesFisicos.questionarioBasal.lazer : {};
		$scope.patientSelected.examesFisicos.questionarioBasal.sumario = $scope.patientSelected.examesFisicos.questionarioBasal.sumario ? $scope.patientSelected.examesFisicos.questionarioBasal.sumario :{};
		parseDate();
	}
	
	function parseDate(){
		if($scope.patientSelected.examesFisicos.avaliacaoFisica.dataAvaliacao != null){
			$scope.patientSelected.examesFisicos.avaliacaoFisica.dataAvaliacao = new Date($scope.patientSelected.examesFisicos.avaliacaoFisica.dataAvaliacao);
			$scope.patientSelected.examesFisicos.avaliacaoFisica.dataAvaliacao.setHours($scope.patientSelected.examesFisicos.avaliacaoFisica.dataAvaliacao.getHours() + 3);
		}
		
	}
	
	$scope.update = function(patient){
		$http.put('rest/patient/updatePatient',patient)
		.then(function(response){
			$scope.patients = response.data.patients;
			$scope.message = "Paciente atualizado com sucesso";
			$scope.patientSelected = null;
		},function(error){
			$scope.message = "Operação falhou";
		})
	};
	
});


