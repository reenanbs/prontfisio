package com.prontfisio.business;

import com.prontfisio.entities.User;
import com.prontfisio.persistence.UserDAO;

public class UserBC {

	
	public void verifyIfExistUserWithLogin(User user) {
		UserDAO userDAO = new UserDAO();
		Long id = userDAO.verifyIfExist(user.getLogin());
		if(id != null) {
			user.setId(id);
			userDAO.update(user);
		}
		
	}
}
