package com.prontfisio.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.prontfisio.entities.Patient;
import com.prontfisio.persistence.PatientDAO;

@Produces("application/json")
@Path("/patient")
public class PatientService {

	@POST
	@Path("/insertPatient")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> insertPatient(Patient patient) {
		Map<String, Object> result = new HashMap<String, Object>();
		if (patient == null) {
			result.put("status", "error");
			return result;
		}
		PatientDAO patientDAO = new PatientDAO();
		try {
			patientDAO.insert(patient);
			result.put("status", "success");
			result.put("patients", patientDAO.findAll());
		} catch (Exception e) {
			result.put("status", "error");
		}
		return result;
	}

	@GET
	@Path("/findAllPatient")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Patient> findAllPatient() {
		List<Patient> patients = new ArrayList<Patient>();
		try {
			patients = new PatientDAO().findAll();
		} catch (Exception e) {

		}
		return patients;
	}

	@PUT
	@Path("/updatePatient")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String, Object> updatePatient(Patient patient) {
		Map<String, Object> result = new HashMap<String, Object>();
		if (patient == null) {
			result.put("status", "error");
			return result;
		}
		PatientDAO patientDAO = new PatientDAO();
		try {
			patientDAO.update(patient);
			result.put("status", "success");
			result.put("patients", patientDAO.findAll());
		} catch (Exception e) {
			result.put("status", "error");

		}
		return result;
	}

	@DELETE
	@Path("/deletePatient/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> deletePatient(@PathParam("id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		PatientDAO patientDAO = new PatientDAO();
		try {
			if (id != null) {
				patientDAO.delete(patientDAO.findById(id));
				result.put("status", "success");
				result.put("message", "Paciente removido com sucesso");
				result.put("patients", patientDAO.findAll());
			}
		} catch (Exception e) {
			result.put("status", "error");
		}
		return result;

	}

	@GET
	@Path("/search")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> search() {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result.put("patients", new PatientDAO().agenda(null));
		} catch (Exception e) {
			
		}
		result.put("status", "success");
		return result;
	}
	
	@POST
	@Path("/searchByDate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> searchByDate(Map<String, Object> map) {
		Map<String, Object> result = new HashMap<String, Object>();
		if(map.get("date") == null) {
			result.put("message", "Dados invalidos");
			return result;
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		Date date = null;
		try {
			date = dateFormat.parse((String) map.get("date"));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		try {
			result.put("patients", new PatientDAO().agenda(date));
		} catch (Exception e) {
			
		}
		result.put("status", "success");
		return result;
	}

}
