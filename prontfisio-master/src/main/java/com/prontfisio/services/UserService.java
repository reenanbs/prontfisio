package com.prontfisio.services;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.prontfisio.business.UserBC;
import com.prontfisio.entities.User;
import com.prontfisio.security.SecurityHelper;

@Produces("application/json")
@Path("/user")
public class UserService {

	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String, Object> update(User user) {
		Map<String, Object> result = new HashMap<String, Object>();
		if (user == null) {
			result.put("status", "error");
			return result;
		}
		UserBC userBC = new UserBC();
		try {
			SecurityHelper securityHelper = new SecurityHelper();
			User userBD = securityHelper.cryptography(user.getLogin(), user.getSenha());
			userBC.verifyIfExistUserWithLogin(userBD);
			result.put("status", "success");
		} catch (Exception e) {
			result.put("status", "error");
			
		}

		return result;
	}
}
