package com.prontfisio.services;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.prontfisio.entities.Physiotherapist;
import com.prontfisio.entities.User;
import com.prontfisio.persistence.PhysiotherapistDAO;
import com.prontfisio.security.SecurityHelper;

@Produces("application/json")
@Path("/physiotherapist")
public class PhysiotherapistService {


	@POST
	@Path("/insertPhysiotherapist")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> insertPhysiotherapist(Physiotherapist physiotherapist) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		if (physiotherapist == null) {
			result.put("status", "error");
			return result;
		}

		PhysiotherapistDAO physiotherapistDAO = new PhysiotherapistDAO();
		try {
			SecurityHelper securityHelper = new SecurityHelper();
			User user = securityHelper.cryptography(physiotherapist.getUser().getLogin(), physiotherapist.getUser().getSenha());
			physiotherapist.setUser(user);
			physiotherapistDAO.insert(physiotherapist);
			result.put("status", "success");
			result.put("physiotherapists", physiotherapistDAO.findAll());
		} catch (Exception e) {
			result.put("status", "error");
		}

		return result;
	}

	@GET
	@Path("/findAllPhysiotherapist")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Physiotherapist> findAllPhysiotherapist() {
		List<Physiotherapist> physiotherapists = new ArrayList<Physiotherapist>();
		try {
			physiotherapists = new PhysiotherapistDAO().findAll();
		} catch (Exception e) {
			
		}

		return physiotherapists;
	}
	
	@PUT
	@Path("/updatePhysiotherapist")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String, Object> updatePhysiotherapist(Physiotherapist physiotherapist) {
		Map<String, Object> result = new HashMap<String, Object>();
		if (physiotherapist == null) {
			result.put("status", "error");
			return result;
		}
		PhysiotherapistDAO physiotherapistDAO = new PhysiotherapistDAO();
		try {
			physiotherapistDAO.update(physiotherapist);
			result.put("status", "success");
			result.put("physiotherapists", physiotherapistDAO.findAll());
		} catch (Exception e) {
			result.put("status", "error");
			
		}

		return result;
	}
	
	@DELETE
	@Path("/deletePhysiotherapist/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> deletePhysiotherapist(@PathParam("id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		PhysiotherapistDAO physiotherapistDAO = new PhysiotherapistDAO();
		try {
			if (id != null) {
				physiotherapistDAO.delete(physiotherapistDAO.findById(id));
				result.put("message", "Fisioterapeuta removido com sucesso");
				result.put("physiotherapists", physiotherapistDAO.findAll());
			}
		} catch (Exception e) {
			
		}
		return result;

	}
	
}
