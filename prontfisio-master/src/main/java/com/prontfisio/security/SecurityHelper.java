package com.prontfisio.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.prontfisio.entities.User;

public class SecurityHelper {

	public User cryptography(String email, String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String login = email.substring(0,email.indexOf("@"));
		String senha = login + password;
		
		MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
		byte messageDigestSenha[] = algorithm.digest(senha.getBytes("UTF-8"));

		StringBuilder hexStringSenha = new StringBuilder();
		for (byte b : messageDigestSenha) {
			hexStringSenha.append(String.format("%02X", 0xFF & b));
		}
		String senhahexAdmin = hexStringSenha.toString();
		
		User user = new User();
		user.setLogin(email);
		user.setSenha(senhahexAdmin);
		return user;
	}
	
}
