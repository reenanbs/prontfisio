package com.prontfisio.servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.prontfisio.entities.User;
import com.prontfisio.persistence.UserDAO;
import com.prontfisio.security.SecurityHelper;


/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter("login");
		String senha = request.getParameter("senha");
		
		if(login.equals("") || senha.equals("") || login == null || senha == null){
			response.sendRedirect("/ProntFisio/loginFisioterapeuta.html");
			return;
		}
		
		SecurityHelper securityHelper = new SecurityHelper();
		User user = null;
		try {
			user = securityHelper.cryptography(login, senha);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		if(user == null) {
			response.sendRedirect("/ProntFisio/loginFisioterapeuta.html");
			return;
		}
		
		User usuario = new UserDAO().login(user.getLogin(), user.getSenha());
		if(usuario == null){
			response.sendRedirect("/ProntFisio/loginFisioterapeuta.html");
			return;
		}
		
		HttpSession session = request.getSession(true);
		session.setAttribute("usuario", usuario);
		
		/*response.sendRedirect("/ProntFisio");*/
		response.sendRedirect("/ProntFisio/agendamento.html");

		
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if(session != null) {
			session.setAttribute("usuario", null);
			session.invalidate();
		}
		response.sendRedirect("/ProntFisio/loginFisioterapeuta.html");
	}
}
