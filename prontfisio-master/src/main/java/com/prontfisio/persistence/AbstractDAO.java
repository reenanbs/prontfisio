package com.prontfisio.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.prontfisio.entities.AbstractEntity;



public abstract class AbstractDAO<T extends AbstractEntity> {
	static EntityManagerFactory fac = Persistence.createEntityManagerFactory("prontFisio");
	EntityManager manager;

	public void insert(T entity) {
		manager = fac.createEntityManager();
		
		try{
			manager.getTransaction().begin();
			manager.persist(entity);
			manager.getTransaction().commit();
		}catch(Exception e){
			manager.getTransaction().rollback();
		}
		finally{
			manager.close();
		}
		
	}

	public void update(T entity) {
		manager = fac.createEntityManager();
		try{
			manager.getTransaction().begin();
			manager.merge(entity);
			manager.getTransaction().commit();
		}catch(Exception e){
			manager.getTransaction().rollback();
		}
		finally{
			manager.close();
		}
	}

	public void delete(T entity) {
		manager = fac.createEntityManager();
		try{
			manager.getTransaction().begin();
			entity = manager.find(getEntityClass(), entity.getId());
			manager.remove(entity);
			manager.getTransaction().commit();
		}catch(Exception e){
			manager.getTransaction().rollback();
		}
		finally{
			manager.close();
		}
		
	}

	public T findById(Long id) {
		manager = fac.createEntityManager();
		try{
			return manager.find(getEntityClass(), id);
		}
		finally{
			manager.close();
		}
	}

	public abstract Class<T> getEntityClass();

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		
		manager = fac.createEntityManager();
		try{
			Query query = manager.createQuery("select c from "
					+ getEntityClass().getSimpleName() + " c");
			return query.getResultList();
		}
		finally{
			manager.close();
		}
		

	}

}
