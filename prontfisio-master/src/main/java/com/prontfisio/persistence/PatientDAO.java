package com.prontfisio.persistence;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TemporalType;

import com.prontfisio.entities.Patient;
import com.prontfisio.helper.DateHelper;

public class PatientDAO extends AbstractDAO<Patient> {

	@Override
	public Class<Patient> getEntityClass() {
		return Patient.class;
	}
	
	
	public List<Patient> agenda(Date date) {
		DateHelper dateHelper = new DateHelper();
		Date firstDayOfWeek = null;
		Date lastDayOfWeek = null;
		if(date == null) {
			firstDayOfWeek = dateHelper.getFirstDayOfWeekCurrent();
			lastDayOfWeek = dateHelper.getLastDayOfWeekCurrent();
		} else {
			firstDayOfWeek = dateHelper.getFirstOrLastDayOfWeekByDate(date, true);
			lastDayOfWeek = dateHelper.getFirstOrLastDayOfWeekByDate(date, false);
		}
		manager = fac.createEntityManager();
		try{
			Query query = manager.createQuery("select t from "
					+ getEntityClass().getSimpleName() + " t WHERE t.evaluationDate BETWEEN :firstDay and :lastDay");
			query.setParameter("firstDay", firstDayOfWeek, TemporalType.DATE);
			query.setParameter("lastDay", lastDayOfWeek, TemporalType.DATE);
			@SuppressWarnings("unchecked")
			List<Patient> list = query.getResultList();
			return list;
		}
		finally{
			manager.close();
		}
		
	}
	
}
