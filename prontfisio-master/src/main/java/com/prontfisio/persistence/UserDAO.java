package com.prontfisio.persistence;

import java.util.List;

import javax.persistence.Query;

import com.prontfisio.entities.User;

public class UserDAO extends AbstractDAO<User> {
	
	@Override
	public Class<User> getEntityClass() {
		return User.class;
	}
	
	public User login(String login, String senha){
		manager = fac.createEntityManager();
		
		Query  query = manager.createQuery("select u from User u where u.login = :login and u.senha = :senha");
		query.setParameter("login", login);
		query.setParameter("senha", senha);
		@SuppressWarnings("unchecked")
		List<User> users = query.getResultList();
		if(users != null && !users.isEmpty()){
			return users.get(0);
		}
		return null;
		
	}
	
	public Long verifyIfExist(String login) {
		manager = fac.createEntityManager();
		Query  query = manager.createQuery("select u from User u where u.login = :login");
		query.setParameter("login", login);
		@SuppressWarnings("unchecked")
		List<User> users = query.getResultList();
		if(users != null && !users.isEmpty()){
			return users.get(0).getId();
		}
		return null;
	}
}
