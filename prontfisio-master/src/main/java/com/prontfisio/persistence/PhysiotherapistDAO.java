package com.prontfisio.persistence;

import com.prontfisio.entities.Physiotherapist;

public class PhysiotherapistDAO extends AbstractDAO<Physiotherapist> {
	
	@Override
	public Class<Physiotherapist> getEntityClass() {
		return Physiotherapist.class;
	}
}
