package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_questionario_basal")
@SequenceGenerator(name="questionario_basal_id", sequenceName="questionario_basal_seq", allocationSize=1 )
public class QuestionarioBasal extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="questionario_basal_id")
	@Column(name = "id_questionario_basal", nullable = false)
	private Long id;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Ocupacao ocupacao;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Esporte esporte;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Lazer lazer;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Sumario sumario;
	
	private String diagnostico;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Ocupacao getOcupacao() {
		return ocupacao;
	}

	public void setOcupacao(Ocupacao ocupacao) {
		this.ocupacao = ocupacao;
	}

	public Esporte getEsporte() {
		return esporte;
	}

	public void setEsporte(Esporte esporte) {
		this.esporte = esporte;
	}

	public Lazer getLazer() {
		return lazer;
	}

	public void setLazer(Lazer lazer) {
		this.lazer = lazer;
	}

	public Sumario getSumario() {
		return sumario;
	}

	public void setSumario(Sumario sumario) {
		this.sumario = sumario;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
	
}