package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_sumario")
@SequenceGenerator(name="sumario_id", sequenceName="sumario_seq", allocationSize=1 )
public class Sumario extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sumario_id")
	@Column(name = "id_sumario", nullable = false)
	private Long id;
	
	private String ocupacional;
	
	private String atividadeEsportiva;
	
	private String atividadeLazer;
	
	private String totalAbsoluto;
	
	private String totalMedio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOcupacional() {
		return ocupacional;
	}

	public void setOcupacional(String ocupacional) {
		this.ocupacional = ocupacional;
	}

	public String getAtividadeEsportiva() {
		return atividadeEsportiva;
	}

	public void setAtividadeEsportiva(String atividadeEsportiva) {
		this.atividadeEsportiva = atividadeEsportiva;
	}

	public String getAtividadeLazer() {
		return atividadeLazer;
	}

	public void setAtividadeLazer(String atividadeLazer) {
		this.atividadeLazer = atividadeLazer;
	}

	public String getTotalAbsoluto() {
		return totalAbsoluto;
	}

	public void setTotalAbsoluto(String totalAbsoluto) {
		this.totalAbsoluto = totalAbsoluto;
	}

	public String getTotalMedio() {
		return totalMedio;
	}

	public void setTotalMedio(String totalMedio) {
		this.totalMedio = totalMedio;
	}
	
	
}