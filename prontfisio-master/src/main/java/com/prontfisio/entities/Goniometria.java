package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_goniometria")
@SequenceGenerator(name="goniometria_id", sequenceName="goniometria_seq", allocationSize=1 )
public class Goniometria extends AbstractEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="goniometria_id")
	@Column(name = "id_goniometria", nullable = false)
	private Long id;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Pescoco pescoco;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Coluna coluna;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Ombro ombro;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Cotovelo cotovelo;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Punho punho;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Quadril quadril;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Joelho joelho;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Tornozelo tornozelo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pescoco getPescoco() {
		return pescoco;
	}

	public void setPescoco(Pescoco pescoco) {
		this.pescoco = pescoco;
	}

	public Coluna getColuna() {
		return coluna;
	}

	public void setColuna(Coluna coluna) {
		this.coluna = coluna;
	}

	public Ombro getOmbro() {
		return ombro;
	}

	public void setOmbro(Ombro ombro) {
		this.ombro = ombro;
	}

	public Cotovelo getCotovelo() {
		return cotovelo;
	}

	public void setCotovelo(Cotovelo cotovelo) {
		this.cotovelo = cotovelo;
	}

	public Punho getPunho() {
		return punho;
	}

	public void setPunho(Punho punho) {
		this.punho = punho;
	}

	public Quadril getQuadril() {
		return quadril;
	}

	public void setQuadril(Quadril quadril) {
		this.quadril = quadril;
	}

	public Joelho getJoelho() {
		return joelho;
	}

	public void setJoelho(Joelho joelho) {
		this.joelho = joelho;
	}

	public Tornozelo getTornozelo() {
		return tornozelo;
	}

	public void setTornozelo(Tornozelo tornozelo) {
		this.tornozelo = tornozelo;
	}
	
}