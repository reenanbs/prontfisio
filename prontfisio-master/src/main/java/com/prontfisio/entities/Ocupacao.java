package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_ocupacao")
@SequenceGenerator(name="ocupacao_id", sequenceName="ocupacao_seq", allocationSize=1 )
public class Ocupacao extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ocupacao_id")
	@Column(name = "id_ocupacao", nullable = false)
	private Long id;
	
	private String principalOcupacao;
	
	private String sentaTrabalho;
	
	private String peTrabalho;
	
	private String andaTrabalho;
	
	private String carregaObjetosPesadosTrabalho;
	
	private String cansado;
	
	private String suor;
	
	private String trabalhoFisicamente;
	
	private String totalIndiceOcupacional;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrincipalOcupacao() {
		return principalOcupacao;
	}

	public void setPrincipalOcupacao(String principalOcupacao) {
		this.principalOcupacao = principalOcupacao;
	}

	public String getSentaTrabalho() {
		return sentaTrabalho;
	}

	public void setSentaTrabalho(String sentaTrabalho) {
		this.sentaTrabalho = sentaTrabalho;
	}

	public String getPeTrabalho() {
		return peTrabalho;
	}

	public void setPeTrabalho(String peTrabalho) {
		this.peTrabalho = peTrabalho;
	}

	public String getAndaTrabalho() {
		return andaTrabalho;
	}

	public void setAndaTrabalho(String andaTrabalho) {
		this.andaTrabalho = andaTrabalho;
	}

	public String getCarregaObjetosPesadosTrabalho() {
		return carregaObjetosPesadosTrabalho;
	}

	public void setCarregaObjetosPesadosTrabalho(String carregaObjetosPesadosTrabalho) {
		this.carregaObjetosPesadosTrabalho = carregaObjetosPesadosTrabalho;
	}

	public String getCansado() {
		return cansado;
	}

	public void setCansado(String cansado) {
		this.cansado = cansado;
	}

	public String getSuor() {
		return suor;
	}

	public void setSuor(String suor) {
		this.suor = suor;
	}

	public String getTrabalhoFisicamente() {
		return trabalhoFisicamente;
	}

	public void setTrabalhoFisicamente(String trabalhoFisicamente) {
		this.trabalhoFisicamente = trabalhoFisicamente;
	}

	public String getTotalIndiceOcupacional() {
		return totalIndiceOcupacional;
	}

	public void setTotalIndiceOcupacional(String totalIndiceOcupacional) {
		this.totalIndiceOcupacional = totalIndiceOcupacional;
	}
	
}