package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_tornozelo")
@SequenceGenerator(name="tornozelo_id", sequenceName="tornozelo_seq", allocationSize=1 )
public class Tornozelo extends AbstractEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tornozelo_id")
	@Column(name = "id_tornozelo", nullable = false)
	private Long id;
	
	private String flexaoDireito;
	
	private String flexaoEsquerdo;
	
	private String flexaoConclusao;
	
	private String flexaoPlantarDireito;
	
	private String flexaoPlantarEsquerdo;
	
	private String flexaoPlantarConclusao;
	
	private String inversaoDireito;
	
	private String inversaoEsquerdo;
	
	private String inversaoConclusao;
	
	private String eversaoDireito;
	
	private String eversaoEsquerdo;
	
	private String eversaoConclusao;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFlexaoDireito() {
		return flexaoDireito;
	}

	public void setFlexaoDireito(String flexaoDireito) {
		this.flexaoDireito = flexaoDireito;
	}

	public String getFlexaoEsquerdo() {
		return flexaoEsquerdo;
	}

	public void setFlexaoEsquerdo(String flexaoEsquerdo) {
		this.flexaoEsquerdo = flexaoEsquerdo;
	}

	public String getFlexaoConclusao() {
		return flexaoConclusao;
	}

	public void setFlexaoConclusao(String flexaoConclusao) {
		this.flexaoConclusao = flexaoConclusao;
	}

	public String getFlexaoPlantarDireito() {
		return flexaoPlantarDireito;
	}

	public void setFlexaoPlantarDireito(String flexaoPlantarDireito) {
		this.flexaoPlantarDireito = flexaoPlantarDireito;
	}

	public String getFlexaoPlantarEsquerdo() {
		return flexaoPlantarEsquerdo;
	}

	public void setFlexaoPlantarEsquerdo(String flexaoPlantarEsquerdo) {
		this.flexaoPlantarEsquerdo = flexaoPlantarEsquerdo;
	}

	public String getFlexaoPlantarConclusao() {
		return flexaoPlantarConclusao;
	}

	public void setFlexaoPlantarConclusao(String flexaoPlantarConclusao) {
		this.flexaoPlantarConclusao = flexaoPlantarConclusao;
	}

	public String getInversaoDireito() {
		return inversaoDireito;
	}

	public void setInversaoDireito(String inversaoDireito) {
		this.inversaoDireito = inversaoDireito;
	}

	public String getInversaoEsquerdo() {
		return inversaoEsquerdo;
	}

	public void setInversaoEsquerdo(String inversaoEsquerdo) {
		this.inversaoEsquerdo = inversaoEsquerdo;
	}

	public String getInversaoConclusao() {
		return inversaoConclusao;
	}

	public void setInversaoConclusao(String inversaoConclusao) {
		this.inversaoConclusao = inversaoConclusao;
	}

	public String getEversaoDireito() {
		return eversaoDireito;
	}

	public void setEversaoDireito(String eversaoDireito) {
		this.eversaoDireito = eversaoDireito;
	}

	public String getEversaoEsquerdo() {
		return eversaoEsquerdo;
	}

	public void setEversaoEsquerdo(String eversaoEsquerdo) {
		this.eversaoEsquerdo = eversaoEsquerdo;
	}

	public String getEversaoConclusao() {
		return eversaoConclusao;
	}

	public void setEversaoConclusao(String eversaoConclusao) {
		this.eversaoConclusao = eversaoConclusao;
	}
	
	
	
}