package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_avaliacao_postural")
@SequenceGenerator(name="avaliacao_postural_id", sequenceName="avaliacao_postural_seq", allocationSize=1 )
public class AvaliacaoPostural extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="avaliacao_postural_id")
	@Column(name = "id_avaliacao_postural", nullable = false)
	private Long id;
	
	private String lateralidade;
	
	private String cabeca;
	
	private String alturaDosOmbros;
	
	private String clavicula;
	
	private String alturaDasMaos;
	
	private String rotacaoTronco;
	
	private String anguloTalles;
	
	private String testeAdams;
	
	private String linhaAlba;
	
	private String cristasIliacas;
	
	private String eias;
	
	private String joelho;
	
	private String patela;
	
	private String tornozelo;
	
	private String pes;
	
	private String halux;
	
	private String posteriorCabeca;
	
	private String posteriorAlturaOmbros;
	
	private String posteriorEscapula;
	
	private String posteriorTesteAdams;
	
	private String posteriorGlutea;
	
	private String poplitea;
	
	private String colunaLombar;
	
	private String colunaToracica;
	
	private String colunaCervical;
	
	private String calcaneo;
	
	private String vistaLateralCabeca;
	
	private String vistaLateralCervical;

	private String vistaLateralOmbros;
	
	private String vistaLateralMao;

	private String vistaLateralColunaToracica;
	
	private String vistaLateralRotacaoTronco;

	private String vistaLateralAbdomem;
	
	private String vistaLateralColunaLombar;

	private String vistaLateralPelve;
	
	private String vistaLateralJoelho;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLateralidade() {
		return lateralidade;
	}

	public void setLateralidade(String lateralidade) {
		this.lateralidade = lateralidade;
	}

	public String getCabeca() {
		return cabeca;
	}

	public void setCabeca(String cabeca) {
		this.cabeca = cabeca;
	}

	public String getAlturaDosOmbros() {
		return alturaDosOmbros;
	}

	public void setAlturaDosOmbros(String alturaDosOmbros) {
		this.alturaDosOmbros = alturaDosOmbros;
	}

	public String getClavicula() {
		return clavicula;
	}

	public void setClavicula(String clavicula) {
		this.clavicula = clavicula;
	}

	public String getAlturaDasMaos() {
		return alturaDasMaos;
	}

	public void setAlturaDasMaos(String alturaDasMaos) {
		this.alturaDasMaos = alturaDasMaos;
	}

	public String getRotacaoTronco() {
		return rotacaoTronco;
	}

	public void setRotacaoTronco(String rotacaoTronco) {
		this.rotacaoTronco = rotacaoTronco;
	}

	public String getAnguloTalles() {
		return anguloTalles;
	}

	public void setAnguloTalles(String anguloTalles) {
		this.anguloTalles = anguloTalles;
	}

	public String getTesteAdams() {
		return testeAdams;
	}

	public void setTesteAdams(String testeAdams) {
		this.testeAdams = testeAdams;
	}

	public String getLinhaAlba() {
		return linhaAlba;
	}

	public void setLinhaAlba(String linhaAlba) {
		this.linhaAlba = linhaAlba;
	}

	public String getCristasIliacas() {
		return cristasIliacas;
	}

	public void setCristasIliacas(String cristasIliacas) {
		this.cristasIliacas = cristasIliacas;
	}

	public String getEias() {
		return eias;
	}

	public void setEias(String eias) {
		this.eias = eias;
	}

	public String getJoelho() {
		return joelho;
	}

	public void setJoelho(String joelho) {
		this.joelho = joelho;
	}

	public String getPatela() {
		return patela;
	}

	public void setPatela(String patela) {
		this.patela = patela;
	}

	public String getTornozelo() {
		return tornozelo;
	}

	public void setTornozelo(String tornozelo) {
		this.tornozelo = tornozelo;
	}

	public String getPes() {
		return pes;
	}

	public void setPes(String pes) {
		this.pes = pes;
	}

	public String getHalux() {
		return halux;
	}

	public void setHalux(String halux) {
		this.halux = halux;
	}

	public String getPosteriorCabeca() {
		return posteriorCabeca;
	}

	public void setPosteriorCabeca(String posteriorCabeca) {
		this.posteriorCabeca = posteriorCabeca;
	}

	public String getPosteriorAlturaOmbros() {
		return posteriorAlturaOmbros;
	}

	public void setPosteriorAlturaOmbros(String posteriorAlturaOmbros) {
		this.posteriorAlturaOmbros = posteriorAlturaOmbros;
	}

	public String getPosteriorEscapula() {
		return posteriorEscapula;
	}

	public void setPosteriorEscapula(String posteriorEscapula) {
		this.posteriorEscapula = posteriorEscapula;
	}

	public String getPosteriorTesteAdams() {
		return posteriorTesteAdams;
	}

	public void setPosteriorTesteAdams(String posteriorTesteAdams) {
		this.posteriorTesteAdams = posteriorTesteAdams;
	}

	public String getPosteriorGlutea() {
		return posteriorGlutea;
	}

	public void setPosteriorGlutea(String posteriorGlutea) {
		this.posteriorGlutea = posteriorGlutea;
	}

	public String getPoplitea() {
		return poplitea;
	}

	public void setPoplitea(String poplitea) {
		this.poplitea = poplitea;
	}

	public String getColunaLombar() {
		return colunaLombar;
	}

	public void setColunaLombar(String colunaLombar) {
		this.colunaLombar = colunaLombar;
	}

	public String getColunaToracica() {
		return colunaToracica;
	}

	public void setColunaToracica(String colunaToracica) {
		this.colunaToracica = colunaToracica;
	}

	public String getColunaCervical() {
		return colunaCervical;
	}

	public void setColunaCervical(String colunaCervical) {
		this.colunaCervical = colunaCervical;
	}

	public String getCalcaneo() {
		return calcaneo;
	}

	public void setCalcaneo(String calcaneo) {
		this.calcaneo = calcaneo;
	}

	public String getVistaLateralCabeca() {
		return vistaLateralCabeca;
	}

	public void setVistaLateralCabeca(String vistaLateralCabeca) {
		this.vistaLateralCabeca = vistaLateralCabeca;
	}

	public String getVistaLateralCervical() {
		return vistaLateralCervical;
	}

	public void setVistaLateralCervical(String vistaLateralCervical) {
		this.vistaLateralCervical = vistaLateralCervical;
	}

	public String getVistaLateralOmbros() {
		return vistaLateralOmbros;
	}

	public void setVistaLateralOmbros(String vistaLateralOmbros) {
		this.vistaLateralOmbros = vistaLateralOmbros;
	}

	public String getVistaLateralMao() {
		return vistaLateralMao;
	}

	public void setVistaLateralMao(String vistaLateralMao) {
		this.vistaLateralMao = vistaLateralMao;
	}

	public String getVistaLateralColunaToracica() {
		return vistaLateralColunaToracica;
	}

	public void setVistaLateralColunaToracica(String vistaLateralColunaToracica) {
		this.vistaLateralColunaToracica = vistaLateralColunaToracica;
	}

	public String getVistaLateralRotacaoTronco() {
		return vistaLateralRotacaoTronco;
	}

	public void setVistaLateralRotacaoTronco(String vistaLateralRotacaoTronco) {
		this.vistaLateralRotacaoTronco = vistaLateralRotacaoTronco;
	}

	public String getVistaLateralAbdomem() {
		return vistaLateralAbdomem;
	}

	public void setVistaLateralAbdomem(String vistaLateralAbdomem) {
		this.vistaLateralAbdomem = vistaLateralAbdomem;
	}

	public String getVistaLateralColunaLombar() {
		return vistaLateralColunaLombar;
	}

	public void setVistaLateralColunaLombar(String vistaLateralColunaLombar) {
		this.vistaLateralColunaLombar = vistaLateralColunaLombar;
	}

	public String getVistaLateralPelve() {
		return vistaLateralPelve;
	}

	public void setVistaLateralPelve(String vistaLateralPelve) {
		this.vistaLateralPelve = vistaLateralPelve;
	}

	public String getVistaLateralJoelho() {
		return vistaLateralJoelho;
	}

	public void setVistaLateralJoelho(String vistaLateralJoelho) {
		this.vistaLateralJoelho = vistaLateralJoelho;
	}
	
	
}