package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_avaliacao_marcha")
@SequenceGenerator(name="avaliacao_marcha_id", sequenceName="avaliacao_marcha_seq", allocationSize=1 )
public class AvaliacaoMarcha extends AbstractEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="avaliacao_marcha_id")
	@Column(name = "id_avaliacao_marcha", nullable = false)
	private Long id;
	
	private String inicioMarcha;

	private String balanceioPernaDireitaPassa;

	private String balanceioPernaDireitaAfasta;

	private String balanceioPernaEsquerdaPassa;

	private String balanceioPernaEsquerdaAfasta;

	private String simetriaPassos;

	private String continuidadePassos;

	private String desvio;

	private String tronco;

	private String baseApoio;

	private String escoreMarcha;
	
	private String escoreTotal;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getInicioMarcha() {
		return inicioMarcha;
	}

	public void setInicioMarcha(String inicioMarcha) {
		this.inicioMarcha = inicioMarcha;
	}

	public String getBalanceioPernaDireitaPassa() {
		return balanceioPernaDireitaPassa;
	}

	public void setBalanceioPernaDireitaPassa(String balanceioPernaDireitaPassa) {
		this.balanceioPernaDireitaPassa = balanceioPernaDireitaPassa;
	}

	public String getBalanceioPernaDireitaAfasta() {
		return balanceioPernaDireitaAfasta;
	}

	public void setBalanceioPernaDireitaAfasta(String balanceioPernaDireitaAfasta) {
		this.balanceioPernaDireitaAfasta = balanceioPernaDireitaAfasta;
	}

	public String getBalanceioPernaEsquerdaPassa() {
		return balanceioPernaEsquerdaPassa;
	}

	public void setBalanceioPernaEsquerdaPassa(String balanceioPernaEsquerdaPassa) {
		this.balanceioPernaEsquerdaPassa = balanceioPernaEsquerdaPassa;
	}

	public String getBalanceioPernaEsquerdaAfasta() {
		return balanceioPernaEsquerdaAfasta;
	}

	public void setBalanceioPernaEsquerdaAfasta(String balanceioPernaEsquerdaAfasta) {
		this.balanceioPernaEsquerdaAfasta = balanceioPernaEsquerdaAfasta;
	}

	public String getSimetriaPassos() {
		return simetriaPassos;
	}

	public void setSimetriaPassos(String simetriaPassos) {
		this.simetriaPassos = simetriaPassos;
	}

	public String getContinuidadePassos() {
		return continuidadePassos;
	}

	public void setContinuidadePassos(String continuidadePassos) {
		this.continuidadePassos = continuidadePassos;
	}

	public String getDesvio() {
		return desvio;
	}

	public void setDesvio(String desvio) {
		this.desvio = desvio;
	}

	public String getTronco() {
		return tronco;
	}

	public void setTronco(String tronco) {
		this.tronco = tronco;
	}

	public String getBaseApoio() {
		return baseApoio;
	}

	public void setBaseApoio(String baseApoio) {
		this.baseApoio = baseApoio;
	}

	public String getEscoreMarcha() {
		return escoreMarcha;
	}

	public void setEscoreMarcha(String escoreMarcha) {
		this.escoreMarcha = escoreMarcha;
	}

	public String getEscoreTotal() {
		return escoreTotal;
	}

	public void setEscoreTotal(String escoreTotal) {
		this.escoreTotal = escoreTotal;
	}
	
	
}

