package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_exame_fisico")
@SequenceGenerator(name="exame_fisico_id", sequenceName="exame_fisico_seq", allocationSize=1 )
public class ExameFisico extends AbstractEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="exame_fisico_id")
	@Column(name = "id_exame_fisico", nullable = false)
	private Long id;
	
	@OneToOne(cascade=CascadeType.ALL)
    private AvaliacaoFisica avaliacaoFisica;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Goniometria goniometria;
	
	@OneToOne(cascade=CascadeType.ALL)
    private ForcaMuscular forcaMuscular;
	
	@OneToOne(cascade=CascadeType.ALL)
    private AvaliacaoPostural avaliacaoPostural;
	
	@OneToOne(cascade=CascadeType.ALL)
    private AvaliacaoEquilibrio avaliacaoEquilibrio;
	
	@OneToOne(cascade=CascadeType.ALL)
    private AvaliacaoMarcha avaliacaoMarcha;
	
	@OneToOne(cascade=CascadeType.ALL)
    private EscalaVisualDeDor escalaVisualDeDor;
	
	@OneToOne(cascade=CascadeType.ALL)
    private QuestionarioBasal questionarioBasal;
	
	private String timeUpGo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTimeUpGo() {
		return timeUpGo;
	}

	public void setTimeUpGo(String timeUpGo) {
		this.timeUpGo = timeUpGo;
	}

	public AvaliacaoFisica getAvaliacaoFisica() {
		return avaliacaoFisica;
	}

	public void setAvaliacaoFisica(AvaliacaoFisica avaliacaoFisica) {
		this.avaliacaoFisica = avaliacaoFisica;
	}

	public Goniometria getGoniometria() {
		return goniometria;
	}

	public void setGoniometria(Goniometria goniometria) {
		this.goniometria = goniometria;
	}

	public ForcaMuscular getForcaMuscular() {
		return forcaMuscular;
	}

	public void setForcaMuscular(ForcaMuscular forcaMuscular) {
		this.forcaMuscular = forcaMuscular;
	}

	public AvaliacaoPostural getAvaliacaoPostural() {
		return avaliacaoPostural;
	}

	public void setAvaliacaoPostural(AvaliacaoPostural avaliacaoPostural) {
		this.avaliacaoPostural = avaliacaoPostural;
	}

	public AvaliacaoEquilibrio getAvaliacaoEquilibrio() {
		return avaliacaoEquilibrio;
	}

	public void setAvaliacaoEquilibrio(AvaliacaoEquilibrio avaliacaoEquilibrio) {
		this.avaliacaoEquilibrio = avaliacaoEquilibrio;
	}

	public AvaliacaoMarcha getAvaliacaoMarcha() {
		return avaliacaoMarcha;
	}

	public void setAvaliacaoMarcha(AvaliacaoMarcha avaliacaoMarcha) {
		this.avaliacaoMarcha = avaliacaoMarcha;
	}

	public EscalaVisualDeDor getEscalaVisualDeDor() {
		return escalaVisualDeDor;
	}

	public void setEscalaVisualDeDor(EscalaVisualDeDor escalaVisualDeDor) {
		this.escalaVisualDeDor = escalaVisualDeDor;
	}

	public QuestionarioBasal getQuestionarioBasal() {
		return questionarioBasal;
	}

	public void setQuestionarioBasal(QuestionarioBasal questionarioBasal) {
		this.questionarioBasal = questionarioBasal;
	}
	
}