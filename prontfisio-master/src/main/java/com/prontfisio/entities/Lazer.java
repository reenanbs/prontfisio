package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_lazer")
@SequenceGenerator(name="lazer_id", sequenceName="lazer_seq", allocationSize=1 )
public class Lazer extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="lazer_id")
	@Column(name = "id_lazer", nullable = false)
	private Long id;
	
	private String tv;
	
	private String andaPe;
	
	private String andaBicileta;
	
	private String minutosAndando;
	
	private String calculoIndiceAtividade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTv() {
		return tv;
	}

	public void setTv(String tv) {
		this.tv = tv;
	}

	public String getAndaPe() {
		return andaPe;
	}

	public void setAndaPe(String andaPe) {
		this.andaPe = andaPe;
	}

	public String getAndaBicileta() {
		return andaBicileta;
	}

	public void setAndaBicileta(String andaBicileta) {
		this.andaBicileta = andaBicileta;
	}

	public String getMinutosAndando() {
		return minutosAndando;
	}

	public void setMinutosAndando(String minutosAndando) {
		this.minutosAndando = minutosAndando;
	}

	public String getCalculoIndiceAtividade() {
		return calculoIndiceAtividade;
	}

	public void setCalculoIndiceAtividade(String calculoIndiceAtividade) {
		this.calculoIndiceAtividade = calculoIndiceAtividade;
	}
	
	
}