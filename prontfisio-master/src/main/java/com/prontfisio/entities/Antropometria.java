package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_antropometria")
@SequenceGenerator(name="antropometria_id", sequenceName="antropometria_seq", allocationSize=1 )
public class Antropometria extends AbstractEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="antropometria_id")
	@Column(name = "id_antropometria", nullable = false)
	private Long id;
	
	private Double peso;
	
	private String cintura;
	
	private String quadril;
	
	private String cinturaQuadril;
	
	private Double altura;
	
	private Double imc;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public String getCintura() {
		return cintura;
	}

	public void setCintura(String cintura) {
		this.cintura = cintura;
	}

	public String getQuadril() {
		return quadril;
	}

	public void setQuadril(String quadril) {
		this.quadril = quadril;
	}

	public String getCinturaQuadril() {
		return cinturaQuadril;
	}

	public void setCinturaQuadril(String cinturaQuadril) {
		this.cinturaQuadril = cinturaQuadril;
	}

	public Double getAltura() {
		return altura;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}

	public Double getImc() {
		return imc;
	}

	public void setImc(Double imc) {
		this.imc = imc;
	}

	
}