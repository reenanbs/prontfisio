package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_punho")
@SequenceGenerator(name="punho_id", sequenceName="punho_seq", allocationSize=1 )
public class Punho extends AbstractEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="punho_id")
	@Column(name = "id_punho", nullable = false)
	private Long id;
	
	private String flexaoDireito;
	
	private String flexaoEsquerdo;
	
	private String flexaoConclusao;
	
	private String extensaoDireito;
	
	private String extensaoEsquerdo;
	
	private String extensaoConclusao;
	
	private String desvioRadialDireito;
	
	private String desvioRadialEsquerdo;
	
	private String desvioRadialConclusao;
	
	private String desvioUlunarDireito;
	
	private String desvioUlunarEsquerdo;
	
	private String desvioUlunarConclusao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFlexaoDireito() {
		return flexaoDireito;
	}

	public void setFlexaoDireito(String flexaoDireito) {
		this.flexaoDireito = flexaoDireito;
	}

	public String getFlexaoEsquerdo() {
		return flexaoEsquerdo;
	}

	public void setFlexaoEsquerdo(String flexaoEsquerdo) {
		this.flexaoEsquerdo = flexaoEsquerdo;
	}

	public String getFlexaoConclusao() {
		return flexaoConclusao;
	}

	public void setFlexaoConclusao(String flexaoConclusao) {
		this.flexaoConclusao = flexaoConclusao;
	}

	public String getExtensaoDireito() {
		return extensaoDireito;
	}

	public void setExtensaoDireito(String extensaoDireito) {
		this.extensaoDireito = extensaoDireito;
	}

	public String getExtensaoEsquerdo() {
		return extensaoEsquerdo;
	}

	public void setExtensaoEsquerdo(String extensaoEsquerdo) {
		this.extensaoEsquerdo = extensaoEsquerdo;
	}

	public String getExtensaoConclusao() {
		return extensaoConclusao;
	}

	public void setExtensaoConclusao(String extensaoConclusao) {
		this.extensaoConclusao = extensaoConclusao;
	}

	public String getDesvioRadialDireito() {
		return desvioRadialDireito;
	}

	public void setDesvioRadialDireito(String desvioRadialDireito) {
		this.desvioRadialDireito = desvioRadialDireito;
	}

	public String getDesvioRadialEsquerdo() {
		return desvioRadialEsquerdo;
	}

	public void setDesvioRadialEsquerdo(String desvioRadialEsquerdo) {
		this.desvioRadialEsquerdo = desvioRadialEsquerdo;
	}

	public String getDesvioRadialConclusao() {
		return desvioRadialConclusao;
	}

	public void setDesvioRadialConclusao(String desvioRadialConclusao) {
		this.desvioRadialConclusao = desvioRadialConclusao;
	}

	public String getDesvioUlunarDireito() {
		return desvioUlunarDireito;
	}

	public void setDesvioUlunarDireito(String desvioUlunarDireito) {
		this.desvioUlunarDireito = desvioUlunarDireito;
	}

	public String getDesvioUlunarEsquerdo() {
		return desvioUlunarEsquerdo;
	}

	public void setDesvioUlunarEsquerdo(String desvioUlunarEsquerdo) {
		this.desvioUlunarEsquerdo = desvioUlunarEsquerdo;
	}

	public String getDesvioUlunarConclusao() {
		return desvioUlunarConclusao;
	}

	public void setDesvioUlunarConclusao(String desvioUlunarConclusao) {
		this.desvioUlunarConclusao = desvioUlunarConclusao;
	}
	
	
}