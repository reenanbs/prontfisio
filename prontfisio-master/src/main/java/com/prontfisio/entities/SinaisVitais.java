package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_sinais_vitais")
@SequenceGenerator(name="sinais_vitais_id", sequenceName="sinais_vitais_seq", allocationSize=1 )
public class SinaisVitais extends AbstractEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sinais_vitais_id")
	@Column(name = "id_sinais_vitais", nullable = false)
	private Long id;
	
	private String temperatura;
	
	private String pressaoArterial;
	
	private String frequenciacardiaca;
	
	private String frequenciaRespiratoria;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}

	public String getPressaoArterial() {
		return pressaoArterial;
	}

	public void setPressaoArterial(String pressaoArterial) {
		this.pressaoArterial = pressaoArterial;
	}

	public String getFrequenciacardiaca() {
		return frequenciacardiaca;
	}

	public void setFrequenciacardiaca(String frequenciacardiaca) {
		this.frequenciacardiaca = frequenciacardiaca;
	}

	public String getFrequenciaRespiratoria() {
		return frequenciaRespiratoria;
	}

	public void setFrequenciaRespiratoria(String frequenciaRespiratoria) {
		this.frequenciaRespiratoria = frequenciaRespiratoria;
	}

	
}