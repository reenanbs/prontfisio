package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_esporte")
@SequenceGenerator(name="esporte_id", sequenceName="esporte_seq", allocationSize=1 )
public class Esporte extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="esporte_id")
	@Column(name = "id_esporte", nullable = false)
	private Long id;
	
	private String esportePraticado;
	
	private String pratica;
	
	private String intensidade;
	
	private String tempo;
	
	private String proporcaoMeses;
	
	private String segundoEsporte;
	
	private String segundoEsporteTempo;
	
	private String segundoEsporteProporcao;
	
	private String calculoIntensidadeTempoProporcao;
	
	private String somaCalculoEsporte;
	
	private String atividadeLazer;
	
	private String suorLazer;
	
	private String esporteLazer;
	
	private String calculoIndiceAtividadeEsportiva;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEsportePraticado() {
		return esportePraticado;
	}

	public void setEsportePraticado(String esportePraticado) {
		this.esportePraticado = esportePraticado;
	}

	public String getPratica() {
		return pratica;
	}

	public void setPratica(String pratica) {
		this.pratica = pratica;
	}

	public String getIntensidade() {
		return intensidade;
	}

	public void setIntensidade(String intensidade) {
		this.intensidade = intensidade;
	}

	public String getTempo() {
		return tempo;
	}

	public void setTempo(String tempo) {
		this.tempo = tempo;
	}

	public String getProporcaoMeses() {
		return proporcaoMeses;
	}

	public void setProporcaoMeses(String proporcaoMeses) {
		this.proporcaoMeses = proporcaoMeses;
	}

	public String getSegundoEsporte() {
		return segundoEsporte;
	}

	public void setSegundoEsporte(String segundoEsporte) {
		this.segundoEsporte = segundoEsporte;
	}

	public String getSegundoEsporteTempo() {
		return segundoEsporteTempo;
	}

	public void setSegundoEsporteTempo(String segundoEsporteTempo) {
		this.segundoEsporteTempo = segundoEsporteTempo;
	}

	public String getSegundoEsporteProporcao() {
		return segundoEsporteProporcao;
	}

	public void setSegundoEsporteProporcao(String segundoEsporteProporcao) {
		this.segundoEsporteProporcao = segundoEsporteProporcao;
	}

	public String getCalculoIntensidadeTempoProporcao() {
		return calculoIntensidadeTempoProporcao;
	}

	public void setCalculoIntensidadeTempoProporcao(String calculoIntensidadeTempoProporcao) {
		this.calculoIntensidadeTempoProporcao = calculoIntensidadeTempoProporcao;
	}

	public String getSomaCalculoEsporte() {
		return somaCalculoEsporte;
	}

	public void setSomaCalculoEsporte(String somaCalculoEsporte) {
		this.somaCalculoEsporte = somaCalculoEsporte;
	}

	public String getAtividadeLazer() {
		return atividadeLazer;
	}

	public void setAtividadeLazer(String atividadeLazer) {
		this.atividadeLazer = atividadeLazer;
	}

	public String getSuorLazer() {
		return suorLazer;
	}

	public void setSuorLazer(String suorLazer) {
		this.suorLazer = suorLazer;
	}

	public String getEsporteLazer() {
		return esporteLazer;
	}

	public void setEsporteLazer(String esporteLazer) {
		this.esporteLazer = esporteLazer;
	}

	public String getCalculoIndiceAtividadeEsportiva() {
		return calculoIndiceAtividadeEsportiva;
	}

	public void setCalculoIndiceAtividadeEsportiva(String calculoIndiceAtividadeEsportiva) {
		this.calculoIndiceAtividadeEsportiva = calculoIndiceAtividadeEsportiva;
	}
	
	
}