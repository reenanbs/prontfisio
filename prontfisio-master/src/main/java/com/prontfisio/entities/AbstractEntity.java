package com.prontfisio.entities;

public abstract class AbstractEntity {
	public abstract Long getId();
	public abstract void setId(Long id);
	
}
