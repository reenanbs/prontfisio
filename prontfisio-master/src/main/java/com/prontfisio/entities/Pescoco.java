package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_pescoco")
@SequenceGenerator(name="pescoco_id", sequenceName="pescoco_seq", allocationSize=1 )
public class Pescoco extends AbstractEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="pescoco_id")
	@Column(name = "id_pescoco", nullable = false)
	private Long id;

	private String flexaoDireito;
	
	private String flexaoEsquerdo;
	
	private String flexaoConclusao;
	
	private String extensaoDireito;
	
	private String extensaoEsquerdo;
	
	private String extensaoConclusao;
	
	private String flexaoLateralDireito;
	
	private String flexaoLateralEsquerdo;
	
	private String flexaoLateralConclusao;
	
	private String rotacaoLateralDireito;
	
	private String rotacaoLateralEsquerdo;
	
	private String rotacaoLateralConclusao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFlexaoDireito() {
		return flexaoDireito;
	}

	public void setFlexaoDireito(String flexaoDireito) {
		this.flexaoDireito = flexaoDireito;
	}

	public String getFlexaoEsquerdo() {
		return flexaoEsquerdo;
	}

	public void setFlexaoEsquerdo(String flexaoEsquerdo) {
		this.flexaoEsquerdo = flexaoEsquerdo;
	}

	public String getFlexaoConclusao() {
		return flexaoConclusao;
	}

	public void setFlexaoConclusao(String flexaoConclusao) {
		this.flexaoConclusao = flexaoConclusao;
	}

	public String getExtensaoDireito() {
		return extensaoDireito;
	}

	public void setExtensaoDireito(String extensaoDireito) {
		this.extensaoDireito = extensaoDireito;
	}

	public String getExtensaoEsquerdo() {
		return extensaoEsquerdo;
	}

	public void setExtensaoEsquerdo(String extensaoEsquerdo) {
		this.extensaoEsquerdo = extensaoEsquerdo;
	}

	public String getExtensaoConclusao() {
		return extensaoConclusao;
	}

	public void setExtensaoConclusao(String extensaoConclusao) {
		this.extensaoConclusao = extensaoConclusao;
	}

	public String getFlexaoLateralDireito() {
		return flexaoLateralDireito;
	}

	public void setFlexaoLateralDireito(String flexaoLateralDireito) {
		this.flexaoLateralDireito = flexaoLateralDireito;
	}

	public String getFlexaoLateralEsquerdo() {
		return flexaoLateralEsquerdo;
	}

	public void setFlexaoLateralEsquerdo(String flexaoLateralEsquerdo) {
		this.flexaoLateralEsquerdo = flexaoLateralEsquerdo;
	}

	public String getFlexaoLateralConclusao() {
		return flexaoLateralConclusao;
	}

	public void setFlexaoLateralConclusao(String flexaoLateralConclusao) {
		this.flexaoLateralConclusao = flexaoLateralConclusao;
	}

	public String getRotacaoLateralDireito() {
		return rotacaoLateralDireito;
	}

	public void setRotacaoLateralDireito(String rotacaoLateralDireito) {
		this.rotacaoLateralDireito = rotacaoLateralDireito;
	}

	public String getRotacaoLateralEsquerdo() {
		return rotacaoLateralEsquerdo;
	}

	public void setRotacaoLateralEsquerdo(String rotacaoLateralEsquerdo) {
		this.rotacaoLateralEsquerdo = rotacaoLateralEsquerdo;
	}

	public String getRotacaoLateralConclusao() {
		return rotacaoLateralConclusao;
	}

	public void setRotacaoLateralConclusao(String rotacaoLateralConclusao) {
		this.rotacaoLateralConclusao = rotacaoLateralConclusao;
	}
	
	
}