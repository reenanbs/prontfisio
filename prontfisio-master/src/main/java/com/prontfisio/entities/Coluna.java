package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_coluna")
@SequenceGenerator(name="coluna_id", sequenceName="coluna_seq", allocationSize=1 )
public class Coluna extends AbstractEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="coluna_id")
	@Column(name = "id_coluna", nullable = false)
	private Long id;

	private String flexaoLombarDireito;
	
	private String flexaoLombarEsquerdo;
	
	private String flexaoLombarConclusao;
	
	private String extensaoLombarDireito;
	
	private String extensaoLombarEsquerdo;
	
	private String extensaoLombarConclusao;
	
	private String flexaoTroncoDireito;
	
	private String flexaoTroncoEsquerdo;
	
	private String flexaoTroncoConclusao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFlexaoLombarDireito() {
		return flexaoLombarDireito;
	}

	public void setFlexaoLombarDireito(String flexaoLombarDireito) {
		this.flexaoLombarDireito = flexaoLombarDireito;
	}

	public String getFlexaoLombarEsquerdo() {
		return flexaoLombarEsquerdo;
	}

	public void setFlexaoLombarEsquerdo(String flexaoLombarEsquerdo) {
		this.flexaoLombarEsquerdo = flexaoLombarEsquerdo;
	}

	public String getFlexaoLombarConclusao() {
		return flexaoLombarConclusao;
	}

	public void setFlexaoLombarConclusao(String flexaoLombarConclusao) {
		this.flexaoLombarConclusao = flexaoLombarConclusao;
	}

	public String getExtensaoLombarDireito() {
		return extensaoLombarDireito;
	}

	public void setExtensaoLombarDireito(String extensaoLombarDireito) {
		this.extensaoLombarDireito = extensaoLombarDireito;
	}

	public String getExtensaoLombarEsquerdo() {
		return extensaoLombarEsquerdo;
	}

	public void setExtensaoLombarEsquerdo(String extensaoLombarEsquerdo) {
		this.extensaoLombarEsquerdo = extensaoLombarEsquerdo;
	}

	public String getExtensaoLombarConclusao() {
		return extensaoLombarConclusao;
	}

	public void setExtensaoLombarConclusao(String extensaoLombarConclusao) {
		this.extensaoLombarConclusao = extensaoLombarConclusao;
	}

	public String getFlexaoTroncoDireito() {
		return flexaoTroncoDireito;
	}

	public void setFlexaoTroncoDireito(String flexaoTroncoDireito) {
		this.flexaoTroncoDireito = flexaoTroncoDireito;
	}

	public String getFlexaoTroncoEsquerdo() {
		return flexaoTroncoEsquerdo;
	}

	public void setFlexaoTroncoEsquerdo(String flexaoTroncoEsquerdo) {
		this.flexaoTroncoEsquerdo = flexaoTroncoEsquerdo;
	}

	public String getFlexaoTroncoConclusao() {
		return flexaoTroncoConclusao;
	}

	public void setFlexaoTroncoConclusao(String flexaoTroncoConclusao) {
		this.flexaoTroncoConclusao = flexaoTroncoConclusao;
	}
	
}