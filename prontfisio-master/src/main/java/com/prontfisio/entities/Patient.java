package com.prontfisio.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tb_patient")
@SequenceGenerator(name="patient_id", sequenceName="patient_seq", allocationSize=1 )
public class Patient extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="patient_id")
	@Column(name = "id_patient", nullable = false)
	private Long id;
	
	@Column(name = "name_patient", nullable = false, length = 100)
	private String name;
	
	@Column(name="evaluation_date", nullable = false)
	@Temporal(value=TemporalType.DATE)
	private Date evaluationDate;
	
	@Column(name="date_of_birth")
	@Temporal(value=TemporalType.DATE)
	private Date dateOfBirth;
	
	@Column(name="rg_patient", length = 20)
	private String rgPatient;
	
	@Column(name="sexo", length=20)
	private String sexo;
	
	@Column(name="status_civil", length=20)
	private String statusCivil;
	
	@Column(name="address", length=150)
	private String address;
	
	@Column(name="phone", length=20)
	private String phone;
	
	@Column(name="schooling",  length=50)
	private String schooling;
	
	@Column(name="occupation",length=100)
	private String occupation;

	@Column(name="revenue", length=20)
	private String revenue;
	
	@Column(name="diagnostic", length=255)
	private String diagnostic;
	
	@Column(name="parentName", length=100)
	private String parentName;
	
	@Column(name="medication", length=100)
	private String medication;
	
	@Column(name="doctorName", length=100)
	private String doctorName;
	
	@Column(name="complaint", length=255)
	private String complaint;
	
	@Column(name="historic", length=255)
	private String historic;
	
	@Column(name="physicalActivity", length=100)
	private String physicalActivity;
	
	@OneToOne(cascade=CascadeType.ALL)
    private SinaisVitais sinaisVitais;
	
	@OneToOne(cascade=CascadeType.ALL)
    private Antropometria antropometria;
	
	@OneToOne(cascade=CascadeType.ALL)
    private ExameFisico examesFisicos;
	
	public Patient() {
		super();
	}

	public Patient(Long id) {
		super();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getEvaluationDate() {
		return evaluationDate;
	}

	public void setEvaluationDate(Date evaluationDate) {
		this.evaluationDate = evaluationDate;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getRgPatient() {
		return rgPatient;
	}

	public void setRgPatient(String rgPatient) {
		this.rgPatient = rgPatient;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getStatusCivil() {
		return statusCivil;
	}

	public void setStatusCivil(String statusCivil) {
		this.statusCivil = statusCivil;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSchooling() {
		return schooling;
	}

	public void setSchooling(String schooling) {
		this.schooling = schooling;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getRevenue() {
		return revenue;
	}

	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}

	public String getDiagnostic() {
		return diagnostic;
	}

	public void setDiagnostic(String diagnostic) {
		this.diagnostic = diagnostic;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getMedication() {
		return medication;
	}

	public void setMedication(String medication) {
		this.medication = medication;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getComplaint() {
		return complaint;
	}

	public void setComplaint(String complaint) {
		this.complaint = complaint;
	}

	public String getHistoric() {
		return historic;
	}

	public void setHistoric(String historic) {
		this.historic = historic;
	}

	public String getPhysicalActivity() {
		return physicalActivity;
	}

	public void setPhysicalActivity(String physicalActivity) {
		this.physicalActivity = physicalActivity;
	}

	public SinaisVitais getSinaisVitais() {
		return sinaisVitais;
	}

	public void setSinaisVitais(SinaisVitais sinaisVitais) {
		this.sinaisVitais = sinaisVitais;
	}
	
	public Antropometria getAntropometria() {
		return antropometria;
	}

	public void setAntropometria(Antropometria antropometria) {
		this.antropometria = antropometria;
	}
	
	public ExameFisico getExamesFisicos() {
		return examesFisicos;
	}

	public void setExamesFisicos(ExameFisico examesFisicos) {
		this.examesFisicos = examesFisicos;
	}

}
