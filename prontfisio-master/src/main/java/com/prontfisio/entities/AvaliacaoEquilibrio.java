package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_avaliacao_equilibrio")
@SequenceGenerator(name="avaliacao_equilibrio_id", sequenceName="avaliacao_equilibrio_seq", allocationSize=1 )
public class AvaliacaoEquilibrio extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="avaliacao_equilibrio_id")
	@Column(name = "id_avaliacao_equilibrio", nullable = false)
	private Long id;
	
	private String equilibrioSentado;
	
	private String levantaDaCadeira;
	
	private String tentativasLevantar;
	
	private String equilibrioImediato;
	
	private String equilibrioPe;

	private String desequilibrio;
	
	private String olhosFechados;

	private String girando;
	
	private String sentar;

	private String total;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEquilibrioSentado() {
		return equilibrioSentado;
	}

	public void setEquilibrioSentado(String equilibrioSentado) {
		this.equilibrioSentado = equilibrioSentado;
	}

	public String getLevantaDaCadeira() {
		return levantaDaCadeira;
	}

	public void setLevantaDaCadeira(String levantaDaCadeira) {
		this.levantaDaCadeira = levantaDaCadeira;
	}

	public String getTentativasLevantar() {
		return tentativasLevantar;
	}

	public void setTentativasLevantar(String tentativasLevantar) {
		this.tentativasLevantar = tentativasLevantar;
	}

	public String getEquilibrioImediato() {
		return equilibrioImediato;
	}

	public void setEquilibrioImediato(String equilibrioImediato) {
		this.equilibrioImediato = equilibrioImediato;
	}

	public String getEquilibrioPe() {
		return equilibrioPe;
	}

	public void setEquilibrioPe(String equilibrioPe) {
		this.equilibrioPe = equilibrioPe;
	}

	public String getDesequilibrio() {
		return desequilibrio;
	}

	public void setDesequilibrio(String desequilibrio) {
		this.desequilibrio = desequilibrio;
	}

	public String getOlhosFechados() {
		return olhosFechados;
	}

	public void setOlhosFechados(String olhosFechados) {
		this.olhosFechados = olhosFechados;
	}

	public String getGirando() {
		return girando;
	}

	public void setGirando(String girando) {
		this.girando = girando;
	}

	public String getSentar() {
		return sentar;
	}

	public void setSentar(String sentar) {
		this.sentar = sentar;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}
	
}