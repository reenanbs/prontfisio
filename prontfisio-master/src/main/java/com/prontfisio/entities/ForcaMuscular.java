package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_forca_muscular")
@SequenceGenerator(name="forca_muscular_id", sequenceName="forca_muscular_seq", allocationSize=1 )
public class ForcaMuscular extends AbstractEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="forca_muscular_id")
	@Column(name = "id_forca_muscular", nullable = false)
	private Long id;
	
	private String elevacaoEscapular;
	
	private String flexaoOmbro;
	
	private String extensaoOmbro;
	
	private String abducaoOmbro;
	
	private String abducaoOmbroDeltoidePosterior;
	
	private String abducaoOmbroDeltoideAnterior;
	
	private String rotacaoInternaOmbro;
	
	private String rotacaoExternaOmbro;
	
	private String flexaoCotoveloSupinacao;
	
	private String flexaoCotoveloPronacao;
	
	private String flexaoCotoveloNeutro;
	
	private String extensaoCotovelo;
	
	private String supinacaoAntebraco;
	
	private String flexaoPunho;
	
	private String extensaoPunho;
	
	private String flexaoMetacarpo;
	
	private String flexaoInterfalangeana;
	
	private String extensaoIndicadorMinimo;
	
	private String abducaoDorsal;
	
	private String abducaoPalmares;
	
	private String flexaoCervical;
	
	private String extensaoCervical;
	
	private String flexaoTronco;
	
	private String extensaoTronco;
	
	private String flexaoQuadril;
	
	private String sartorio;
	
	private String extensaoQuadril;
	
	private String abducaoQuadril;
	
	private String aducaoQuadril;
	
	private String rotacaoInternaQuadril;
	
	private String rotacaoExternaQuadril;
	
	private String extensaoFemoral;
	
	private String flexaoSemitendinoso;
	
	private String flexaoPlantarTornozelo;
	
	private String flexaoPlantarTornozeloSoleo;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getElevacaoEscapular() {
		return elevacaoEscapular;
	}

	public void setElevacaoEscapular(String elevacaoEscapular) {
		this.elevacaoEscapular = elevacaoEscapular;
	}

	public String getFlexaoOmbro() {
		return flexaoOmbro;
	}

	public void setFlexaoOmbro(String flexaoOmbro) {
		this.flexaoOmbro = flexaoOmbro;
	}

	public String getExtensaoOmbro() {
		return extensaoOmbro;
	}

	public void setExtensaoOmbro(String extensaoOmbro) {
		this.extensaoOmbro = extensaoOmbro;
	}

	public String getAbducaoOmbro() {
		return abducaoOmbro;
	}

	public void setAbducaoOmbro(String abducaoOmbro) {
		this.abducaoOmbro = abducaoOmbro;
	}

	public String getAbducaoOmbroDeltoidePosterior() {
		return abducaoOmbroDeltoidePosterior;
	}

	public void setAbducaoOmbroDeltoidePosterior(String abducaoOmbroDeltoidePosterior) {
		this.abducaoOmbroDeltoidePosterior = abducaoOmbroDeltoidePosterior;
	}

	public String getAbducaoOmbroDeltoideAnterior() {
		return abducaoOmbroDeltoideAnterior;
	}

	public void setAbducaoOmbroDeltoideAnterior(String abducaoOmbroDeltoideAnterior) {
		this.abducaoOmbroDeltoideAnterior = abducaoOmbroDeltoideAnterior;
	}

	public String getRotacaoInternaOmbro() {
		return rotacaoInternaOmbro;
	}

	public void setRotacaoInternaOmbro(String rotacaoInternaOmbro) {
		this.rotacaoInternaOmbro = rotacaoInternaOmbro;
	}

	public String getRotacaoExternaOmbro() {
		return rotacaoExternaOmbro;
	}

	public void setRotacaoExternaOmbro(String rotacaoExternaOmbro) {
		this.rotacaoExternaOmbro = rotacaoExternaOmbro;
	}

	public String getFlexaoCotoveloSupinacao() {
		return flexaoCotoveloSupinacao;
	}

	public void setFlexaoCotoveloSupinacao(String flexaoCotoveloSupinacao) {
		this.flexaoCotoveloSupinacao = flexaoCotoveloSupinacao;
	}

	public String getFlexaoCotoveloPronacao() {
		return flexaoCotoveloPronacao;
	}

	public void setFlexaoCotoveloPronacao(String flexaoCotoveloPronacao) {
		this.flexaoCotoveloPronacao = flexaoCotoveloPronacao;
	}

	public String getFlexaoCotoveloNeutro() {
		return flexaoCotoveloNeutro;
	}

	public void setFlexaoCotoveloNeutro(String flexaoCotoveloNeutro) {
		this.flexaoCotoveloNeutro = flexaoCotoveloNeutro;
	}

	public String getExtensaoCotovelo() {
		return extensaoCotovelo;
	}

	public void setExtensaoCotovelo(String extensaoCotovelo) {
		this.extensaoCotovelo = extensaoCotovelo;
	}

	public String getSupinacaoAntebraco() {
		return supinacaoAntebraco;
	}

	public void setSupinacaoAntebraco(String supinacaoAntebraco) {
		this.supinacaoAntebraco = supinacaoAntebraco;
	}

	public String getFlexaoPunho() {
		return flexaoPunho;
	}

	public void setFlexaoPunho(String flexaoPunho) {
		this.flexaoPunho = flexaoPunho;
	}

	public String getExtensaoPunho() {
		return extensaoPunho;
	}

	public void setExtensaoPunho(String extensaoPunho) {
		this.extensaoPunho = extensaoPunho;
	}

	public String getFlexaoMetacarpo() {
		return flexaoMetacarpo;
	}

	public void setFlexaoMetacarpo(String flexaoMetacarpo) {
		this.flexaoMetacarpo = flexaoMetacarpo;
	}

	public String getFlexaoInterfalangeana() {
		return flexaoInterfalangeana;
	}

	public void setFlexaoInterfalangeana(String flexaoInterfalangeana) {
		this.flexaoInterfalangeana = flexaoInterfalangeana;
	}

	public String getExtensaoIndicadorMinimo() {
		return extensaoIndicadorMinimo;
	}

	public void setExtensaoIndicadorMinimo(String extensaoIndicadorMinimo) {
		this.extensaoIndicadorMinimo = extensaoIndicadorMinimo;
	}

	public String getAbducaoDorsal() {
		return abducaoDorsal;
	}

	public void setAbducaoDorsal(String abducaoDorsal) {
		this.abducaoDorsal = abducaoDorsal;
	}

	public String getAbducaoPalmares() {
		return abducaoPalmares;
	}

	public void setAbducaoPalmares(String abducaoPalmares) {
		this.abducaoPalmares = abducaoPalmares;
	}

	public String getFlexaoCervical() {
		return flexaoCervical;
	}

	public void setFlexaoCervical(String flexaoCervical) {
		this.flexaoCervical = flexaoCervical;
	}

	public String getExtensaoCervical() {
		return extensaoCervical;
	}

	public void setExtensaoCervical(String extensaoCervical) {
		this.extensaoCervical = extensaoCervical;
	}

	public String getFlexaoTronco() {
		return flexaoTronco;
	}

	public void setFlexaoTronco(String flexaoTronco) {
		this.flexaoTronco = flexaoTronco;
	}

	public String getExtensaoTronco() {
		return extensaoTronco;
	}

	public void setExtensaoTronco(String extensaoTronco) {
		this.extensaoTronco = extensaoTronco;
	}

	public String getFlexaoQuadril() {
		return flexaoQuadril;
	}

	public void setFlexaoQuadril(String flexaoQuadril) {
		this.flexaoQuadril = flexaoQuadril;
	}

	public String getSartorio() {
		return sartorio;
	}

	public void setSartorio(String sartorio) {
		this.sartorio = sartorio;
	}

	public String getExtensaoQuadril() {
		return extensaoQuadril;
	}

	public void setExtensaoQuadril(String extensaoQuadril) {
		this.extensaoQuadril = extensaoQuadril;
	}

	public String getAbducaoQuadril() {
		return abducaoQuadril;
	}

	public void setAbducaoQuadril(String abducaoQuadril) {
		this.abducaoQuadril = abducaoQuadril;
	}

	public String getAducaoQuadril() {
		return aducaoQuadril;
	}

	public void setAducaoQuadril(String aducaoQuadril) {
		this.aducaoQuadril = aducaoQuadril;
	}

	public String getRotacaoInternaQuadril() {
		return rotacaoInternaQuadril;
	}

	public void setRotacaoInternaQuadril(String rotacaoInternaQuadril) {
		this.rotacaoInternaQuadril = rotacaoInternaQuadril;
	}

	public String getRotacaoExternaQuadril() {
		return rotacaoExternaQuadril;
	}

	public void setRotacaoExternaQuadril(String rotacaoExternaQuadril) {
		this.rotacaoExternaQuadril = rotacaoExternaQuadril;
	}

	public String getExtensaoFemoral() {
		return extensaoFemoral;
	}

	public void setExtensaoFemoral(String extensaoFemoral) {
		this.extensaoFemoral = extensaoFemoral;
	}

	public String getFlexaoSemitendinoso() {
		return flexaoSemitendinoso;
	}

	public void setFlexaoSemitendinoso(String flexaoSemitendinoso) {
		this.flexaoSemitendinoso = flexaoSemitendinoso;
	}

	public String getFlexaoPlantarTornozelo() {
		return flexaoPlantarTornozelo;
	}

	public void setFlexaoPlantarTornozelo(String flexaoPlantarTornozelo) {
		this.flexaoPlantarTornozelo = flexaoPlantarTornozelo;
	}

	public String getFlexaoPlantarTornozeloSoleo() {
		return flexaoPlantarTornozeloSoleo;
	}

	public void setFlexaoPlantarTornozeloSoleo(String flexaoPlantarTornozeloSoleo) {
		this.flexaoPlantarTornozeloSoleo = flexaoPlantarTornozeloSoleo;
	}
	
	
}