package com.prontfisio.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tb_avaliacao_fisica")
@SequenceGenerator(name="avaliacao_fisica_id", sequenceName="avaliacao_fisica_seq", allocationSize=1 )
public class AvaliacaoFisica extends AbstractEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="avaliacao_fisica_id")
	@Column(name = "id_avaliacao_fisica", nullable = false)
	private Long id;
	
	
	private String inspecao;
	
	private String palpacao;
	
	private String pontoReferencia;
	
	@Temporal(value=TemporalType.DATE)
	private Date dataAvaliacao;
	
	private String perimetroDezoitoD;
	
	private String perimetroDezoitoE;
	
	private String perimetroNoveD;
	
	private String perimetroNoveE;
	
	private String supraDireito;
	
	private String supraEsquerdo;
	
	private String infraDireito;
	
	private String infraEsquerdo;
	
	private String noveDireito;
	
	private String noveEsquerdo;
	
	private String dezoitoDireito;
	
	private String dezoitoEsquerdo;
	
	private String vinteSeteDireito;
	
	private String vinteSeteEsquerdo;
	
	private String maleoloDireito;
	
	private String maleoloEsquerdo;
	
	private String aparenteDireito;
	
	private String aparenteEsquerdo;
	
	private String realDireito;
	
	private String realEsquerdo;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getInspecao() {
		return inspecao;
	}

	public void setInspecao(String inspecao) {
		this.inspecao = inspecao;
	}

	public String getPalpacao() {
		return palpacao;
	}

	public void setPalpacao(String palpacao) {
		this.palpacao = palpacao;
	}

	public String getPontoReferencia() {
		return pontoReferencia;
	}

	public void setPontoReferencia(String pontoReferencia) {
		this.pontoReferencia = pontoReferencia;
	}

	public Date getDataAvaliacao() {
		return dataAvaliacao;
	}

	public void setDataAvaliacao(Date dataAvaliacao) {
		this.dataAvaliacao = dataAvaliacao;
	}

	public String getPerimetroDezoitoD() {
		return perimetroDezoitoD;
	}

	public void setPerimetroDezoitoD(String perimetroDezoitoD) {
		this.perimetroDezoitoD = perimetroDezoitoD;
	}

	public String getPerimetroDezoitoE() {
		return perimetroDezoitoE;
	}

	public void setPerimetroDezoitoE(String perimetroDezoitoE) {
		this.perimetroDezoitoE = perimetroDezoitoE;
	}

	public String getPerimetroNoveD() {
		return perimetroNoveD;
	}

	public void setPerimetroNoveD(String perimetroNoveD) {
		this.perimetroNoveD = perimetroNoveD;
	}

	public String getPerimetroNoveE() {
		return perimetroNoveE;
	}

	public void setPerimetroNoveE(String perimetroNoveE) {
		this.perimetroNoveE = perimetroNoveE;
	}

	public String getSupraDireito() {
		return supraDireito;
	}

	public void setSupraDireito(String supraDireito) {
		this.supraDireito = supraDireito;
	}

	public String getSupraEsquerdo() {
		return supraEsquerdo;
	}

	public void setSupraEsquerdo(String supraEsquerdo) {
		this.supraEsquerdo = supraEsquerdo;
	}

	public String getInfraDireito() {
		return infraDireito;
	}

	public void setInfraDireito(String infraDireito) {
		this.infraDireito = infraDireito;
	}

	public String getInfraEsquerdo() {
		return infraEsquerdo;
	}

	public void setInfraEsquerdo(String infraEsquerdo) {
		this.infraEsquerdo = infraEsquerdo;
	}

	public String getNoveDireito() {
		return noveDireito;
	}

	public void setNoveDireito(String noveDireito) {
		this.noveDireito = noveDireito;
	}

	public String getNoveEsquerdo() {
		return noveEsquerdo;
	}

	public void setNoveEsquerdo(String noveEsquerdo) {
		this.noveEsquerdo = noveEsquerdo;
	}

	public String getDezoitoDireito() {
		return dezoitoDireito;
	}

	public void setDezoitoDireito(String dezoitoDireito) {
		this.dezoitoDireito = dezoitoDireito;
	}

	public String getDezoitoEsquerdo() {
		return dezoitoEsquerdo;
	}

	public void setDezoitoEsquerdo(String dezoitoEsquerdo) {
		this.dezoitoEsquerdo = dezoitoEsquerdo;
	}

	public String getVinteSeteDireito() {
		return vinteSeteDireito;
	}

	public void setVinteSeteDireito(String vinteSeteDireito) {
		this.vinteSeteDireito = vinteSeteDireito;
	}

	public String getVinteSeteEsquerdo() {
		return vinteSeteEsquerdo;
	}

	public void setVinteSeteEsquerdo(String vinteSeteEsquerdo) {
		this.vinteSeteEsquerdo = vinteSeteEsquerdo;
	}

	public String getMaleoloDireito() {
		return maleoloDireito;
	}

	public void setMaleoloDireito(String maleoloDireito) {
		this.maleoloDireito = maleoloDireito;
	}

	public String getMaleoloEsquerdo() {
		return maleoloEsquerdo;
	}

	public void setMaleoloEsquerdo(String maleoloEsquerdo) {
		this.maleoloEsquerdo = maleoloEsquerdo;
	}

	public String getAparenteDireito() {
		return aparenteDireito;
	}

	public void setAparenteDireito(String aparenteDireito) {
		this.aparenteDireito = aparenteDireito;
	}

	public String getAparenteEsquerdo() {
		return aparenteEsquerdo;
	}

	public void setAparenteEsquerdo(String aparenteEsquerdo) {
		this.aparenteEsquerdo = aparenteEsquerdo;
	}

	public String getRealDireito() {
		return realDireito;
	}

	public void setRealDireito(String realDireito) {
		this.realDireito = realDireito;
	}

	public String getRealEsquerdo() {
		return realEsquerdo;
	}

	public void setRealEsquerdo(String realEsquerdo) {
		this.realEsquerdo = realEsquerdo;
	}
	
}