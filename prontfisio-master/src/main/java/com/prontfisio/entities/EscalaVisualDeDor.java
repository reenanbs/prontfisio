package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_escala_visual")
@SequenceGenerator(name="escala_visual_id", sequenceName="escala_visual_seq", allocationSize=1 )
public class EscalaVisualDeDor extends AbstractEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="escala_visual_id")
	@Column(name = "id_escala_visual", nullable = false)
	private Long id;
	
	private String rangeEscala;
	
	private String escala;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRangeEscala() {
		return rangeEscala;
	}

	public void setRangeEscala(String rangeEscala) {
		this.rangeEscala = rangeEscala;
	}

	public String getEscala() {
		return escala;
	}

	public void setEscala(String escala) {
		this.escala = escala;
	}
	
}