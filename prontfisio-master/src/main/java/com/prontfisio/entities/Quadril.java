package com.prontfisio.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_quadril")
@SequenceGenerator(name="quadril_id", sequenceName="quadril_seq", allocationSize=1 )
public class Quadril extends AbstractEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="quadril_id")
	@Column(name = "id_quadril", nullable = false)
	private Long id;

	private String flexaoDireito;
	
	private String flexaoEsquerdo;
	
	private String flexaoConclusao;
	
	private String extensaoDireito;
	
	private String extensaoEsquerdo;
	
	private String extensaoConclusao;
	
	private String rotacaoInternaDireito;
	
	private String rotacaoInternaEsquerdo;
	
	private String rotacaoInternaConclusao;
	
	private String rotacaoExternaDireito;
	
	private String rotacaoExternaEsquerdo;
	
	private String rotacaoExternaConclusao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFlexaoDireito() {
		return flexaoDireito;
	}

	public void setFlexaoDireito(String flexaoDireito) {
		this.flexaoDireito = flexaoDireito;
	}

	public String getFlexaoEsquerdo() {
		return flexaoEsquerdo;
	}

	public void setFlexaoEsquerdo(String flexaoEsquerdo) {
		this.flexaoEsquerdo = flexaoEsquerdo;
	}

	public String getFlexaoConclusao() {
		return flexaoConclusao;
	}

	public void setFlexaoConclusao(String flexaoConclusao) {
		this.flexaoConclusao = flexaoConclusao;
	}

	public String getExtensaoDireito() {
		return extensaoDireito;
	}

	public void setExtensaoDireito(String extensaoDireito) {
		this.extensaoDireito = extensaoDireito;
	}

	public String getExtensaoEsquerdo() {
		return extensaoEsquerdo;
	}

	public void setExtensaoEsquerdo(String extensaoEsquerdo) {
		this.extensaoEsquerdo = extensaoEsquerdo;
	}

	public String getExtensaoConclusao() {
		return extensaoConclusao;
	}

	public void setExtensaoConclusao(String extensaoConclusao) {
		this.extensaoConclusao = extensaoConclusao;
	}

	public String getRotacaoInternaDireito() {
		return rotacaoInternaDireito;
	}

	public void setRotacaoInternaDireito(String rotacaoInternaDireito) {
		this.rotacaoInternaDireito = rotacaoInternaDireito;
	}

	public String getRotacaoInternaEsquerdo() {
		return rotacaoInternaEsquerdo;
	}

	public void setRotacaoInternaEsquerdo(String rotacaoInternaEsquerdo) {
		this.rotacaoInternaEsquerdo = rotacaoInternaEsquerdo;
	}

	public String getRotacaoInternaConclusao() {
		return rotacaoInternaConclusao;
	}

	public void setRotacaoInternaConclusao(String rotacaoInternaConclusao) {
		this.rotacaoInternaConclusao = rotacaoInternaConclusao;
	}

	public String getRotacaoExternaDireito() {
		return rotacaoExternaDireito;
	}

	public void setRotacaoExternaDireito(String rotacaoExternaDireito) {
		this.rotacaoExternaDireito = rotacaoExternaDireito;
	}

	public String getRotacaoExternaEsquerdo() {
		return rotacaoExternaEsquerdo;
	}

	public void setRotacaoExternaEsquerdo(String rotacaoExternaEsquerdo) {
		this.rotacaoExternaEsquerdo = rotacaoExternaEsquerdo;
	}

	public String getRotacaoExternaConclusao() {
		return rotacaoExternaConclusao;
	}

	public void setRotacaoExternaConclusao(String rotacaoExternaConclusao) {
		this.rotacaoExternaConclusao = rotacaoExternaConclusao;
	}
	
}