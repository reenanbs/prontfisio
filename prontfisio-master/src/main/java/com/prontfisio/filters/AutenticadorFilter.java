package com.prontfisio.filters;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AutenticadorFilter implements Filter {
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req  = (HttpServletRequest) request;
		HttpServletResponse resp  = (HttpServletResponse) response;
		
		String loginURL = req.getContextPath() + "/loginFisioterapeuta.html";

		if (req.getRequestURI().equals(loginURL)) {
			/*resp.sendRedirect("/ProntFisio/loginFisioterapeuta.html");
			return;*/
			chain.doFilter(request, response);
		}
		else {
			if(req.getSession(false) == null || req.getSession().getAttribute("usuario") == null){
				resp.sendRedirect("/ProntFisio/loginFisioterapeuta.html");
				return;
			}
			chain.doFilter(request, response);
		}
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
